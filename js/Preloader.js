
BasicGame.Preloader = function (game) {

	this.background = null;
	this.preloadBar = null;

	this.ready = false;

};

BasicGame.Preloader.prototype = {

	preload: function () {

		 // Load the 'map.json' file using the TILDED_JSON special flag
					this.game.load.tilemap('map1', 'assets/level12.json', null, Phaser.Tilemap.TILED_JSON);
					this.game.load.tilemap('map2', 'assets/level22.json', null, Phaser.Tilemap.TILED_JSON);
					this.game.load.tilemap('map3', 'assets/level23.json', null, Phaser.Tilemap.TILED_JSON);
					this.game.load.tilemap('map4', 'assets/level24.json', null, Phaser.Tilemap.TILED_JSON);
					this.game.load.tilemap('map5', 'assets/level25.json', null, Phaser.Tilemap.TILED_JSON);
                    this.game.load.tilemap('map6', 'assets/challenging1.json', null, Phaser.Tilemap.TILED_JSON);
					this.game.load.tilemap('map7', 'assets/secondLevel.json', null, Phaser.Tilemap.TILED_JSON);
					this.game.load.tilemap('map8', 'assets/level3.json', null, Phaser.Tilemap.TILED_JSON);
					
                    // Load the image 'level.png' and associate it in the cache as 'level'
                    this.game.load.image('level', 'assets/level2.png');
					this.game.load.spritesheet('glowstick','assets/glowstick.png', 12, 4);
					this.game.load.spritesheet('door2','assets/door2.png', 26, 96);
					this.game.load.spritesheet('sDoor2','assets/sDoor2.png', 96, 21);
                    this.game.load.audio('doorHum','assets/doorHum.mp3')
                    this.game.load.image('pressurePlate', 'assets/pressurePlate.png');
					this.game.load.image('lvl1msg','assets/lvl1msg.png');
					this.game.load.spritesheet('lvl1msg21','assets/lvl1msg2.png', 443, 148);
					this.game.load.spritesheet('lvl1msg22','assets/lvl1msg22.png', 443, 148);
					this.game.load.spritesheet('lvl1msg23','assets/lvl1msg23.png', 443, 148);
					this.game.load.spritesheet('lvl1msg24','assets/lvl1msg24.png', 443, 148);
					this.game.load.spritesheet('lvl1msg25','assets/lvl1msg25.png', 443, 148);
					this.game.load.spritesheet('lvl1msg26','assets/lvl1msg26.png', 443, 148);
                    this.game.load.image('door', 'assets/door.png');
					this.game.load.image('sDoor', 'assets/sDoor.png');
					this.game.load.image('end', 'assets/endpoint.png');
					
					this.game.load.image('pauseMenu','assets/pause.png');
					this.game.load.image('pauseLabel', 'assets/pauseLabel.png')

                    // Load the spritesheet 'character.png', telling Phaser each frame is 30x48
                   this.game.load.spritesheet('character', 'assets/spritesheet2U.png', 30, 49);
				   this.game.load.spritesheet('mover', 'assets/movingPlate.png', 144, 6);
                    this.game.load.spritesheet('crate', 'assets/crate.png', 48, 48);
					this.game.load.spritesheet('crateH', 'assets/crateH.png', 48, 48);
                   this.game.load.image("background", "assets/backdrop3.png");
				   this.game.load.image("backgroundLvl1", "assets/backdropLvl1.png");
				   this.game.load.image("backgroundLvl2", "assets/backdropLvl2.png");
				   this.game.load.image("backgroundLvl3", "assets/backdropLvl3.png");
				   this.game.load.image("backgroundLvl4", "assets/backdropLvl4.png");
				    this.game.load.image("backgroundLvl5", "assets/backdropLvl5.png");
                    //game.load.spritesheet('character', 'assets/spritesheet.png', 111, 49);
					this.game.load.spritesheet('startButton', 'assets/playBtn.png', 170, 47);

	},

	create: function () {

        console.log("Creating preload state!");

		this.state.start('MainMenu');

	}

};
