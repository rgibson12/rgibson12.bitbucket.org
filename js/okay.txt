
BasicGame.Game = function (game) {

	//	When a State is added to Phaser it automatically has the following properties set on it, even if they already exist:

    this.game;		//	a reference to the currently running game
    this.add;		//	used to add sprites, text, groups, etc
    this.camera;	//	a reference to the game camera
    this.cache;		//	the game cache
    this.input;		//	the global input manager (you can access this.input.keyboard, this.input.mouse, as well from it)
    this.load;		//	for preloading assets
    this.math;		//	lots of useful common math operations
    this.sound;		//	the sound manager - add a sound, play one, set-up markers, etc
    this.stage;		//	the game stage
    this.time;		//	the clock
    this.tweens;    //  the tween manager
    this.state;	    //	the state manager
    this.world;		//	the game world
    this.particles;	//	the particle manager
    this.physics;	//	the physics manager
    this.rnd;		//	the repeatable random number generator
    
               this.map;
               this.newMap;
               this.posRotation =0;
               this.reversed = false;
                this.rotR;
                this.orientation = 0;
                this.rotL;
                this.rotValue =0;
                this.layer; // A layer within a tileset
                this.crate1;
                this.player; // The player-controller sprite
               this.facing = "left"; // Which direction the character is facing (default is 'left')
                this.cursors; // A reference to the keys to use for input detection
                this.jumpButton; // A reference to the button used for jumping
                this.grav;
                this.plate1;
                this.opened = false;
                this.time;
                this.hozMove = 120; // The amount to move horizontally
                this.vertMove = -300; // The amount to move vertically (when 'jumping')
                this.jumpTimer = 0; 
                this.gravTimer = 0;
                this.rectangle;
                this.door1;
                this.anotherTimer =0;
                  this.anotherTimer2 =0;
                this.rotTimer = 0;// The initial value of the timer

    //	You can use any of these from any function within this State.
    //	But do consider them as being 'reserved words', i.e. don't create a property for your own game called "world" or you'll over-write the world reference.

};

BasicGame.Game.prototype = {
                

	create: function () {
            
       
      
        console.log("Creating game state!");
       // this.game.add.tileSprite(0,0, 624, 624, 'background', 0);
        //this.game.world.pivot.setTo(50, 50);
        //console.log(this.game.world.pivot);
        
                    this.game.physics.startSystem(Phaser.Physics.P2JS);
                    this.game.physics.startSystem(Phaser.Physics.ARCADE);
                    map = this.game.add.tilemap('map');
                    map.addTilesetImage('level');

                    layer = map.createLayer('Tile Layer 1');
                    layer.resizeWorld();
                    
                    map.setCollisionBetween(1, 6);
                    
                    
                    
                 this.game.physics.p2.convertTilemap(map, layer);
                 map.setTileIndexCallback(7, this.pressPlate, this);
                
                  
                    //door1 = this.game.add.sprite(330 ,(9 *48)+3, 48, 11, 96, 'door');
                    player = this.game.add.sprite(2 * 48, 6 * 48, 'character');
                    crate1 = this.game.add.sprite(3 * 48, 350, 'crate');
                    plate1 = this.game.add.tileSprite(312 ,(9 *48)+3, 48, 7, 'pressurePlate');
                    
                    this.game.physics.p2.enable(player);
                    this.game.physics.p2.enable(crate1);
                    this.game.physics.p2.enable(plate1);
                    
                    //player.body.angle = 90;
                    player.body.fixedRotation = false;
                    crate1.body.fixedRotation = true;
                    plate1.body.fixedRotation = true;
                    
                    

                    player.animations.add('runR',[2,3,4,3],6,true);
                    player.animations.add('runL',[5,6,7,6],6,true);
                    player.animations.add('stand',[0]);

                    player.animations.add('runRU',[10,11,12,11],6,true);
                    player.animations.add('runLU',[13,14,15,14],6,true);
                    player.animations.add('standU',[8]);
                    this.game.world.setBounds(-480, -480, 960, 960);
                    this.game.world.pivot.setTo(480, 480);
                    
                   
                   // player.body.bounce.y = 0;
                   this.game.physics.p2.gravity.y = 380;
                    //this.game.physics.p2.gravity.x = 10000;
                    this.game.camera.follow(player);
                    
                    //this.game.physics.p2.setBoundsToWorld(true, true, true, true, false);                 
                    plate1.body.static = true;
                    this.createDoor();
                    plate1.body.onBeginContact.add(this.openDoor, this);
                    cursors = this.game.input.keyboard.createCursorKeys();
                    grav = this.game.input.keyboard.addKey(Phaser.Keyboard.G);
                    rotR = this.game.input.keyboard.addKey(Phaser.Keyboard.D);
                    rotL = this.game.input.keyboard.addKey(Phaser.Keyboard.A);

                    jumpButton = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
                        
		//	Honestly, just about anything could go here. It's YOUR game after all. Eat your heart out!

	},
        
        openDoor: function(sprite) {
             this.doorTimer = this.game.time.now + 3000;
        },
        
        createDoor: function(){
              door1 = this.game.add.tileSprite(234 ,(2 *48), 11, 96, 'door'); 
           this.game.physics.p2.enable(door1);
            door1.body.fixedRotation = true;
            door1.body.static = true;
        },
        
	update: function () {

                if (this.doorTimer > this.game.time.now && this.opened === false){
                this.game.physics.p2.removeBody(door1.body); 
                door1.destroy();
                  //physics body still remains?!
                  this.opened = true;
                  plate1.body.y += 2;
                }
                else if (this.doorTimer < this.game.time.now && this.opened === true){
                    this.createDoor();
                    this.opened = false;
                    plate1.body.y -= 2;
                }
		//this.game.physics.p2.collide(player, layer);
                    player.body.velocity.x = 0;
                    // Check if the left arrow key is being pressed
                    
                    if (rotR.isDown && this.rotTimer < this.game.time.now){
                        this.time = this.game.time.now;  
                          this.anotherTimer = this.game.time.create(true);
                        switch (this.orientation){
                            case 0:
                                this.game.physics.p2.gravity.y = 0;
                                this.game.physics.p2.gravity.x = 0;
                                this.anotherTimer.loop(1, this.rotateRight, this);
                                this.anotherTimer.start();
                               
                                this.rotTimer = this.game.time.now + 1000;
                                break;
                            case 1:
                                this.game.physics.p2.gravity.y = 0;
                                this.game.physics.p2.gravity.x = 0;
                                this.anotherTimer.loop(1, this.rotateRight, this);
                                this.anotherTimer.start();
                                
//                     
                                this.rotTimer = this.game.time.now + 1000;
                                break;
                            case 2:
                                this.game.physics.p2.gravity.y = 0;
                                this.game.physics.p2.gravity.x = 0;
                                this.anotherTimer.loop(1, this.rotateRight, this);
                                this.anotherTimer.start();
                                
                                this.rotTimer = this.game.time.now + 1000;
                                break;
                            case 3:
                                this.game.physics.p2.gravity.y = 0;
                                this.game.physics.p2.gravity.x = 0;

                                this.anotherTimer.loop(1, this.rotateRight, this);
                                this.anotherTimer.start();
                                
                                this.rotTimer = this.game.time.now + 1000;
                                break;
                        }

                    }
                    else  if (rotL.isDown && this.rotTimer < this.game.time.now){
                        this.anotherTimer2 = this.game.time.create(true);
                           switch (this.orientation){
                            case 0:
                                this.game.physics.p2.gravity.y = 0;
                                this.game.physics.p2.gravity.x = 0;
                                this.anotherTimer2.loop(1, this.rotateLeft, this);
                                this.anotherTimer2.start();
                                
                                this.rotTimer = this.game.time.now + 1000;
                                break;
                            case 1:
                                this.game.physics.p2.gravity.y = 0;
                                this.game.physics.p2.gravity.x = 0;
                                this.anotherTimer2.loop(1, this.rotateLeft, this);
                                this.anotherTimer2.start();
                                
                                this.rotTimer = this.game.time.now + 1000;
                                break;
                            case 2:
                                this.game.physics.p2.gravity.y = 0;
                                this.game.physics.p2.gravity.x = 0;
                                this.anotherTimer2.loop(1, this.rotateLeft, this);
                                this.anotherTimer2.start();
                                
                                this.rotTimer = this.game.time.now + 1000;
                                break;
                            case 3:
                                this.game.physics.p2.gravity.y = 0;
                                this.game.physics.p2.gravity.x = 0;
                               this.anotherTimer2.loop(1, this.rotateLeft, this);
                                this.anotherTimer2.start();
                                
                                this.rotTimer = this.game.time.now + 1000;
                                break;
                        }
               
                    }
                    
                    if (grav.isDown && this.gravTimer < this.game.time.now){
                        if (this.reversed === false){
                            this.reversed = true;}
                        else{this.reversed = false;}
                        switch (this.orientation){
                           case 0:
                               this.game.physics.p2.gravity.y = this.game.physics.p2.gravity.y * -1;
                               this.game.physics.p2.gravity.x =0;
                               break;
                           case 1:
                               this.game.physics.p2.gravity.y = 0;
                               this.game.physics.p2.gravity.x = this.game.physics.p2.gravity.x * -1;
                               break;
                           case 2:
                               this.game.physics.p2.gravity.y = this.game.physics.p2.gravity.y * -1;
                               this.game.physics.p2.gravity.x =0;
                               break;
                           case 3:
                               this.game.physics.p2.gravity.y = 0;
                               this.game.physics.p2.gravity.x = this.game.physics.p2.gravity.x * -1;
                               break;
                        }
                        this.vertMove = this.vertMove * -1; 
                        this.gravTimer = this.game.time.now + 1000;
                       
                       //player.body.bottom = -player.body.bottom;
                       
                       
                       //BOTTOM IS READ ONLY !!
                    }
                    
                    if (cursors.left.isDown)
                    {
                        switch (this.orientation) {
                            case 0:
                                player.body.moveLeft(200);
                                break;
                            case 1:
                                player.body.moveDown(200);
                                break;
                            case 2:
                                player.body.moveRight(200);
                                break;
                            case 3:
                                player.body.moveUp(200);
                                break;    
                        }
                        
                        //player.body.velocity.x = -this.hozMove;
                        if(this.reversed === false){
                        // Set the 'player' sprite's x velocity to a negative number:
                        // have it move left on the screen.
                        
                        player.animations.play('runL');
                        }
                        else{
                          
                        player.animations.play('runLU');  
                        }
                        facing = "left";
                    }
                    // Check if the right arrow key is being pressed
                    else if (cursors.right.isDown)
                    {   
                        switch (this.orientation) {
                            case 0:
                                player.body.moveRight(200);
                                break;
                            case 1:
                                player.body.moveUp(200);
                                break;
                            case 2:
                                player.body.moveLeft(200);
                                break;
                            case 3:
                                player.body.moveDown(200);
                                break;    
                        }
                        if(this.reversed === false){
                        // Set the 'player' sprite's x velocity to a negative number:
                        // have it move left on the screen.
                        
                        player.animations.play('runR');
                        }
                        else{
                          
                        player.animations.play('runRU');  
                        }
                     
                        facing = "right";
                    }
                    else{
                        if (this.reversed === false){
                        player.body.velocity.x=0;
                        player.animations.play('stand');
                        if (this.facing === "left") {
                        
                            player.frame = 1;
                        } else {
                            player.frame = 0;
                        }
                        
                    }
                    else{
                            player.body.velocity.x=0;
                        player.animations.play('standU');
                        if (this.facing === "left") {
                        
                            player.frame = 8;
                        } else {
                            player.frame = 9;
                        }
                        }
                    }
                                   
                                                //inner loop checks whether body is in contact with a surface on top or bottom - prevents double jumping
                                                // body.bottom cant be modified (read only) so using the onFloor method wouldnt work when gravity is reversed
                    if (jumpButton.isDown && this.game.time.now > this.jumpTimer)
                    {
                        
                        player.body.velocity.y = this.vertMove;
                       
                        this.jumpTimer = this.game.time.now + 650;
                         
                        
                    }
                    

	},
        
        rotateRight: function () {
            switch (this.orientation){
                case 0:
                    if (this.posRotation < 1.57){
                        player.body.velocity.x=0;
                        crate1.body.velocity.x = 0;
                        player.body.velocity.y=0;
                        crate1.body.velocity.y=0;
                        this.game.world.rotation += 0.02 ;
                        this.posRotation += 0.02;
                    } 
                    else
                    {
                        this.anotherTimer.destroy();
                        this.orientation = 1;
               
                        player.body.angle -= 90;        
                        player.body.velocity.x=0;
                        crate1.body.velocity.x = 0;
                        player.body.velocity.y=0;
                        crate1.body.velocity.y=0;
                        this.game.world.rotation = 1.571;
                        this.game.physics.p2.gravity.x = 380;
                        this.game.physics.p2.gravity.y = 0;
                        this.rotTimer = this.game.time.now + 1000;                
                    }
                    break;
                case 1:
                    if (this.posRotation < 3.135){
                        player.body.velocity.x=0;
                        crate1.body.velocity.x = 0;
                        player.body.velocity.y=0;
                        crate1.body.velocity.y=0;
                        this.game.world.rotation += 0.02 ;
                        this.posRotation += 0.02;
                    }    
                    else
                    {
                        this.anotherTimer.destroy();
                        this.orientation = 2;
                
                        player.body.angle -= 90; 
                        player.body.velocity.x=0;
                        crate1.body.velocity.x = 0;
                        player.body.velocity.y=0;
                        crate1.body.velocity.y=0;
                        this.game.world.rotation = 3.142;
                        this.game.physics.p2.gravity.x = 0;
                        this.game.physics.p2.gravity.y = -380;
                        this.rotTimer = this.game.time.now + 1000;   
                    }
                    break;
                case 2:
                    if (this.posRotation < 4.7){
                        player.body.velocity.x=0;
                        crate1.body.velocity.x = 0;
                        player.body.velocity.y=0;
                        crate1.body.velocity.y=0;
                        this.game.world.rotation += 0.02 ;
                        this.posRotation += 0.02;
                    }                         
                    else
                    {
                        this.anotherTimer.destroy();
                        this.orientation = 3;
                        this.game.world.rotation = 4.713;
                        player.body.angle -= 90; 
                        player.body.velocity.x=0;
                        crate1.body.velocity.x = 0;
                        player.body.velocity.y=0;
                        crate1.body.velocity.y=0;
                
                        this.game.physics.p2.gravity.x = -10000;
                        this.game.physics.p2.gravity.y = 0;
                        this.rotTimer = this.game.time.now + 1000;
                    }
                    break;
                case 3:
                    if (this.posRotation > 0){
                        this.game.world.rotation += 0.02;
                        this.posRotation += -0.06;
                    }
                    else
                    {
                        this.anotherTimer.destroy();
                        this.orientation = 0;
                        this.posRotation = 0 ;
                        this.game.world.rotation = 0;
                
                        player.body.angle -= player.body.angle;
                        player.body.velocity.x=0;
                        crate1.body.velocity.x = 0;
                        player.body.velocity.y=0;
                        crate1.body.velocity.y=0;
                
                        this.game.physics.p2.gravity.x = 0;
                        this.game.physics.p2.gravity.y = 380;
                        this.rotTimer = this.game.time.now + 1000;               
                    }
                    break;
                }
        },

        
        rotateLeft: function () {
            switch(this.orientation){
                case 0:
                    if (this.posRotation < 4.713){
                        player.body.velocity.x=0;
                        crate1.body.velocity.x = 0;
                        player.body.velocity.y=0;
                        crate1.body.velocity.y=0;
                        this.game.world.rotation += -0.02 ;
                        this.posRotation += 0.06;
                    } 
                    else
                    {
                        this.anotherTimer2.destroy();
                        this.orientation = 3;
               
                        player.body.angle += 90;        
                        player.body.velocity.x=0;
                        crate1.body.velocity.x = 0;
                        player.body.velocity.y=0;
                        crate1.body.velocity.y=0;
                        this.game.world.rotation = 4.713;
                        this.game.physics.p2.gravity.x = -10000;
                        this.game.physics.p2.gravity.y = 0;
                        this.rotTimer = this.game.time.now + 1000;                
                    }
                break;
            
                case 1:
                    if (this.posRotation > 0){
                        this.game.world.rotation += -0.02;
                        this.posRotation += -0.02;
                    }
                    else
                    {
                        this.anotherTimer2.destroy();
                        this.orientation = 0;
                        this.posRotation = 0 ;
                        this.game.world.rotation = 0;
                        player.body.angle -= player.body.angle;
                        player.body.velocity.x=0;
                        crate1.body.velocity.x = 0;
                        player.body.velocity.y=0;
                        crate1.body.velocity.y=0;
                
                        this.game.physics.p2.gravity.x = 0;
                        this.game.physics.p2.gravity.y = 380;
                        this.rotTimer = this.game.time.now + 1000;               
                    }
                    break;
                case 2:
                    if (this.posRotation > 1.57 ){
                        player.body.velocity.x=0;
                        crate1.body.velocity.x = 0;
                        player.body.velocity.y=0;
                        crate1.body.velocity.y=0;
                        this.game.world.rotation += -0.02 ;
                        this.posRotation += -0.02;
                    }                         
                    else
                    {
                        this.anotherTimer2.destroy();
                        this.orientation = 1;
                        this.game.world.rotation = 1.571;
                        player.body.angle += 90;  
                        player.body.velocity.x=0;
                        crate1.body.velocity.x = 0;
                        player.body.velocity.y=0;
                        crate1.body.velocity.y=0;
                
                        this.game.physics.p2.gravity.x = 10000;
                        this.game.physics.p2.gravity.y = 0;
                        this.rotTimer = this.game.time.now + 1000;
                    }
                    break;
                case 3:
                    if (this.posRotation > 3.14){
                        player.body.velocity.x=0;
                        crate1.body.velocity.x = 0;
                        player.body.velocity.y=0;
                        crate1.body.velocity.y=0;
                        this.game.world.rotation += -0.02 ;
                        this.posRotation += -0.02;
                    }    
                    else
                    {
                        this.anotherTimer2.destroy();
                        this.orientation = 2;
                        this.game.world.rotation = 3.142;
                        player.body.angle += 90;  
                        player.body.velocity.x=0;
                        crate1.body.velocity.x = 0;
                        player.body.velocity.y=0;
                        crate1.body.velocity.y=0;
                        
                        this.game.physics.p2.gravity.x = 0;
                        this.game.physics.p2.gravity.y = -380;
                        this.rotTimer = this.game.time.now + 1000;   
                    }
                    break;
            }
            
        },
    render: function () {

		//	Here you should destroy anything you no longer need.
		//	Stop music, delete sprites, purge caches, free resources, all that good stuff.

		//	Then let's go back to the main menu.
		//this.game.debug.body(player);

	},
	quitGame: function (pointer) {

		//	Here you should destroy anything you no longer need.
		//	Stop music, delete sprites, purge caches, free resources, all that good stuff.

		//	Then let's go back to the main menu.
		this.state.start('MainMenu');

	}

};


