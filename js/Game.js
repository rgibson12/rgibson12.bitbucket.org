
BasicGame.Game = function (game) {

	//	When a State is added to Phaser it automatically has the following properties set on it, even if they already exist:

    this.game;		//	a reference to the currently running game
    this.add;		//	used to add sprites, text, groups, etc
    this.camera;	//	a reference to the game camera
    this.cache;		//	the game cache
    this.input;		//	the global input manager (you can access this.input.keyboard, this.input.mouse, as well from it)
    this.load;		//	for preloading assets
    this.math;		//	lots of useful common math operations
    this.sound;		//	the sound manager - add a sound, play one, set-up markers, etc
    this.stage;		//	the game stage
    this.time;		//	the clock
    this.tweens;    //  the tween manager
    this.state;	    //	the state manager
    this.world;		//	the game world
    this.particles;	//	the particle manager
    this.physics;	//	the physics manager
    this.rnd;		//	the repeatable random number generator
    
               this.map;
               this.newMap;
               this.posRotation =0;
               this.reversed = false;
                this.rotR;
				this.glowstick;
				this.pickupTimer=0;
                this.orientation = 0;
				this.needsToMove = false;
                this.rotL;
                this.rotValue =0;
                this.layer; // A layer within a tileset
                this.crate1;
                this.player; // The player-controller sprite
               this.facing = "left"; // Which direction the character is facing (default is 'left')
                this.cursors; // A reference to the keys to use for input detection
                this.jumpButton; // A reference to the button used for jumping
				this.crateDistance;
                this.grav;
                this.plate1;
				this.crate2;
				this.crate3;
				this.door5;
                this.opened = true;
                this.hozMove = 120; // The amount to move horizontally
                this.vertMove = -300; // The amount to move vertically (when 'jumping')
                this.jumpTimer = 0; 
                this.gravTimer = 0;
                this.rectangle;
                this.door1;
				this.door3;
				this.door4;
                this.side = false;
				this.crateBtn;
				this.door2;
				this.endpoint;
                this.door1Opened = false;
                this.door2Opened = false;
				this.door3Opened = false;
                this.door4Opened = false;
                this.plate2;
				this.plate3;
				this.plate4;
				this.onMover;
				this.bodies;
				this.doorTimer = 0;
				this.locked = false;
                this.anotherTimer =0;
                  this.anotherTimer2 =0;
				  this.mover1;
				  this.mover2;
				  this.shadowTexture;
				  this.torch;
                this.rotTimer = 0;// The initial value of the timer
				this.LIGHT_RADIUS;
				this.lightTimer=0;
				this.menu;
				this.choiseLabel;
				this.pauseBtn;
				this.doorhum;
				this.lvl1msg;
				this.background;
				this.msgPlayed = false;
				//this.countdown;
				
    //	You can use any of these from any function within this State.
    //	But do consider them as being 'reserved words', i.e. don't create a property for your own game called "world" or you'll over-write the world reference.

};

BasicGame.Game.prototype = {
                

	create: function () {
		
		
        console.log("Creating game state!");      
                    this.game.physics.startSystem(Phaser.Physics.P2JS);
                    map = this.game.add.tilemap('map'+BasicGame.current_level_id);
					// map = this.game.add.tilemap('map3');
                    map.addTilesetImage('level');

                    layer = map.createLayer('Tile Layer 1');
                    layer.resizeWorld();
                    
                    map.setCollisionBetween(1, 23);
                    
                    
                    
                 this.game.physics.p2.convertTilemap(map, layer);
                 map.setTileIndexCallback(7, this.pressPlate, this);
                
                  
                    //door1 = this.game.add.sprite(330 ,(9 *48)+3, 48, 11, 96, 'door');
					switch(BasicGame.current_level_id){
						case 1:
						background = this.game.add.tileSprite(0, 0, 960, 960, "backgroundLvl1");
							player = this.game.add.sprite(5 * 48, 10 * 48, 'character');
							
							lvl1msg =  this.game.add.sprite(300 ,(4 *48)+3, 'lvl1msg2'+BasicGame.current_level_id);
							lvl1msg.animations.add('play',[0,1,2,3,4], 2, false);
							endpoint = this.game.add.tileSprite(18*48 ,(10*48), 11, 96, 'end');
						
							this.game.world.setBounds(-480, -480, 960, 960);
							this.game.world.pivot.setTo(480, 480);
							doorhum = this.game.add.audio('doorHum');
							                    this.game.physics.p2.enable(player);
                    
	
					
					this.game.physics.p2.enable(endpoint);
                    
                    //player.body.angle = 90;
                    player.body.fixedRotation = false;
                    
					
					endpoint.body.static = true;
					
					
                    
					
					
                    player.animations.add('runR',[2,3,4,3],6,true);
                    player.animations.add('runL',[5,6,7,6],6,true);
                    player.animations.add('stand',[0]);

                    player.animations.add('runRU',[10,11,12,11],6,true);
                    player.animations.add('runLU',[13,14,15,14],6,true);
                    player.animations.add('standU',[8]);
                    
                    
                   
                   // player.body.bounce.y = 0;
                   
                   this.game.physics.p2.gravity.y = 380;
                    //this.game.physics.p2.gravity.x = 10000;
                    this.game.camera.follow(player);
                    
                    //this.game.physics.p2.setBoundsToWorld(true, true, true, true, false);                 
                    
                    
                    
					
					 
					
					this.bodies = this.game.add.group();
					//var body = this.game.add.sprite(player);
					this.bodies.add(player);
					//this.bodies.add(crate1.body);
					
							break;
						case 2:
						this.game.add.tileSprite(0,0, 960, 960, 'backgroundLvl2', 0);
						map = this.game.add.tilemap('map'+BasicGame.current_level_id);
							// map = this.game.add.tilemap('map3');
							map.addTilesetImage('level');

							layer = map.createLayer('Tile Layer 1');
							layer.resizeWorld();
                    
							map.setCollisionBetween(1, 23);
							player = this.game.add.sprite(6 * 48, 10 * 48, 'character');
							lvl1msg =  this.game.add.sprite(400 ,(4 *48)+3, 'lvl1msg2'+BasicGame.current_level_id);
							lvl1msg.animations.add('play',[0,1,2,3,4], 2, false);
							endpoint = this.game.add.tileSprite(8*48 ,(2*48), 11, 96, 'end');
						
							this.game.world.setBounds(-480, -480, 960, 960);
							this.game.world.pivot.setTo(480, 480);
							doorhum = this.game.add.audio('doorHum');
							this.game.physics.p2.enable(player);

							this.game.physics.p2.enable(endpoint);
                    
							player.body.fixedRotation = false;
                    
					
							endpoint.body.static = true;
					
					
                    
					
					
							player.animations.add('runR',[2,3,4,3],6,true);
							player.animations.add('runL',[5,6,7,6],6,true);
							player.animations.add('stand',[0]);

							player.animations.add('runRU',[10,11,12,11],6,true);
							player.animations.add('runLU',[13,14,15,14],6,true);
							player.animations.add('standU',[8]);
                    
                    
                   
							// player.body.bounce.y = 0;
                   
							this.game.physics.p2.gravity.y = 380;
								//this.game.physics.p2.gravity.x = 10000;
							this.game.camera.follow(player);
                    
							//this.game.physics.p2.setBoundsToWorld(true, true, true, true, false);                 
                    
                    
                    
					
					 
					
							this.bodies = this.game.add.group();
							//var body = this.game.add.sprite(player);
							this.bodies.add(player);
							//this.bodies.add(crate1.body);
					
							break;
						case 3:
												this.game.add.tileSprite(0,0, 960, 960, 'backgroundLvl3', 0);
						map = this.game.add.tilemap('map'+BasicGame.current_level_id);
							// map = this.game.add.tilemap('map3');
							map.addTilesetImage('level');
							lvl1msg =  this.game.add.sprite(400 ,(4 *48)+3, 'lvl1msg2'+BasicGame.current_level_id);
							lvl1msg.animations.add('play',[0,1,2,3,4], 2, false);
							layer = map.createLayer('Tile Layer 1');
							layer.resizeWorld();
                    
							map.setCollisionBetween(1, 23);
							player = this.game.add.sprite(5 * 48, 14 * 48, 'character');
							
							endpoint = this.game.add.tileSprite(19*48 ,(17*48), 11, 96, 'end');
							crate1 = this.game.add.sprite(7 * 48, 15*48, 'crate');
							plate2 = this.game.add.tileSprite(73 ,(18 *48)+3, 48, 7, 'pressurePlate');
							this.game.world.setBounds(-480, -480, 960, 960);
							this.game.world.pivot.setTo(480, 480);
							doorhum = this.game.add.audio('doorHum');
							this.game.physics.p2.enable(player);
                    
							this.createDoor1();
							
							this.game.physics.p2.enable(endpoint);
                    
							//player.body.angle = 90;
							player.body.fixedRotation = false;
                    
					this.game.physics.p2.enable(plate2);
					this.game.physics.p2.enable(crate1);
					 plate2.body.static = true;
					 plate2.body.fixedRotation = true;
					 crate1.body.fixedRotation = true;
					endpoint.body.static = true;
					
					
                    
					
					
                    player.animations.add('runR',[2,3,4,3],6,true);
                    player.animations.add('runL',[5,6,7,6],6,true);
                    player.animations.add('stand',[0]);

                    player.animations.add('runRU',[10,11,12,11],6,true);
                    player.animations.add('runLU',[13,14,15,14],6,true);
                    player.animations.add('standU',[8]);
                    
                   
                   
                   // player.body.bounce.y = 0;
                   
                   this.game.physics.p2.gravity.y = 380;
                    //this.game.physics.p2.gravity.x = 10000;
                    this.game.camera.follow(player);
                    
                    //this.game.physics.p2.setBoundsToWorld(true, true, true, true, false);                 
                    
                    
                    
					plate2.body.onBeginContact.add(this.openDoor2, this);
					 
					
					this.bodies = this.game.add.group();
					//var body = this.game.add.sprite(player);
					this.bodies.add(player);
					//this.bodies.add(crate1.body);
					
							break;
						case 4:
						this.game.add.tileSprite(0,0, 960, 960, 'backgroundLvl4', 0);
						map = this.game.add.tilemap('map'+BasicGame.current_level_id);
							// map = this.game.add.tilemap('map3');
							map.addTilesetImage('level');
							lvl1msg =  this.game.add.sprite(260 ,(2 *48)+3, 'lvl1msg2'+BasicGame.current_level_id);
							lvl1msg.animations.add('play',[0,1,2,3,4], 2, false);
							layer = map.createLayer('Tile Layer 1');
							layer.resizeWorld();
                    
							map.setCollisionBetween(1, 23);
						player = this.game.add.sprite(5 * 48, 11 * 48, 'character');
							mover1 = this.game.add.sprite(7.5*48, (8 *48)+10, 'mover');
							endpoint = this.game.add.tileSprite(17*48 ,(11*48), 11, 96, 'end');
							crate1 = this.game.add.sprite(8 * 48, 10*48, 'crate');
							plate2 = this.game.add.tileSprite(73+(8*48) ,(7 *48)+3, 48, 7, 'pressurePlate');
							this.game.world.setBounds(-480, -480, 960, 960);
							this.game.world.pivot.setTo(480, 480);
							doorhum = this.game.add.audio('doorHum');
							                    this.game.physics.p2.enable(player);
                     this.game.physics.p2.enable(mover1);
							this.createDoor1();
							mover1.body.fixedRotation = true;
                    
					mover1.body.static = true;
					this.game.physics.p2.enable(endpoint);
                    mover1.animations.add("moving",[0,1,2],3, true);
                    //player.body.angle = 90;
                    player.body.fixedRotation = false;
                    
					this.game.physics.p2.enable(plate2);
					this.game.physics.p2.enable(crate1);
					 plate2.body.static = true;
					 plate2.body.fixedRotation = true;
					 crate1.body.fixedRotation = true;
					endpoint.body.static = true;
					
					
                    
					
					
                    player.animations.add('runR',[2,3,4,3],6,true);
                    player.animations.add('runL',[5,6,7,6],6,true);
                    player.animations.add('stand',[0]);

                    player.animations.add('runRU',[10,11,12,11],6,true);
                    player.animations.add('runLU',[13,14,15,14],6,true);
                    player.animations.add('standU',[8]);
                    
                    
                   
                   // player.body.bounce.y = 0;
                   
                   this.game.physics.p2.gravity.y = 380;
                    //this.game.physics.p2.gravity.x = 10000;
                    this.game.camera.follow(player);
                    
                    //this.game.physics.p2.setBoundsToWorld(true, true, true, true, false);                 
                    
                    
                    
					plate2.body.onBeginContact.add(this.openDoor2, this);
					 mover1.body.onBeginContact.add(this.move, this);
					mover1.body.onEndContact.add(this.stopMove, this);
					
					this.bodies = this.game.add.group();
					//var body = this.game.add.sprite(player);
					this.bodies.add(player);
					//this.bodies.add(crate1.body);
					
							break;
							case 5:
							this.game.add.tileSprite(0,0, 960, 960, 'backgroundLvl5', 0);
						map = this.game.add.tilemap('map'+BasicGame.current_level_id);
							// map = this.game.add.tilemap('map3');
							map.addTilesetImage('level');

							layer = map.createLayer('Tile Layer 1');
							layer.resizeWorld();
                    
							map.setCollisionBetween(1, 23);
							player = this.game.add.sprite(10 * 48, 11 * 48, 'character');
							glowstick = this.game.add.sprite(10*48, 6*48, 'glowstick');
							crate1 = this.game.add.sprite(12 * 48, 5*48, 'crate');
							
							plate1 = this.game.add.tileSprite(312-3*48 ,(4 *48)+3, 48, 7, 'pressurePlate');
							plate2 = this.game.add.tileSprite(312 ,(12 *48)+3, 48, 7, 'pressurePlate');
							
							endpoint = this.game.add.tileSprite(804-96-40 ,(8 *48), 11, 96, 'end');
							
							//mover2 = this.game.add.sprite(8*48, (1 *48)-3, 'mover');
							doorhum = this.game.add.audio('doorHum');
							this.createDoor1();
							this.createDoor2();
							
							this.game.world.setBounds(-480, -480, 960, 960);
							this.game.world.pivot.setTo(480, 480);
							
							 this.game.physics.p2.enable(player);
							 this.game.physics.p2.enable(glowstick);
                    this.game.physics.p2.enable(crate1);
					
                    this.game.physics.p2.enable(plate1);
                    this.game.physics.p2.enable(plate2);
				
					//this.game.physics.p2.enable(mover2);
					this.game.physics.p2.enable(endpoint);
                    
                    //player.body.angle = 90;
                    player.body.fixedRotation = false;
					glowstick.body.fixedRotation = false;
                    crate1.body.fixedRotation = true;
					
                    plate1.body.fixedRotation = true;
                    plate2.body.fixedRotation = true;
					
					
                    plate1.body.static = true;
                    plate2.body.static = true;
					
					endpoint.body.static = true;
					
					//mover2.body.static = true;
                    
					
					//mover2.animations.add("moving",[0, 1, 2],3, true);
                    player.animations.add('runR',[2,3,4,3],6,true);
                    player.animations.add('runL',[5,6,7,6],6,true);
                    player.animations.add('stand',[0]);

                    player.animations.add('runRU',[10,11,12,11],6,true);
                    player.animations.add('runLU',[13,14,15,14],6,true);
                    player.animations.add('standU',[8]);
                    
                    
                   
                   // player.body.bounce.y = 0;
                   
                   this.game.physics.p2.gravity.y = 380;
                    //this.game.physics.p2.gravity.x = 10000;
                    this.game.camera.follow(player);
                    
                    //this.game.physics.p2.setBoundsToWorld(true, true, true, true, false);                 
                    
                    
                    plate1.body.onBeginContact.add(this.openDoor1, this);
                    plate2.body.onBeginContact.add(this.openDoor2, this);
					
					 
			
					//mover2.body.onBeginContact.add(this.move, this);
					//mover2.body.onEndContact.add(this.stopMove, this);
					
										this.bodies = this.game.add.group();
					//var body = this.game.add.sprite(player);
					this.bodies.add(player);
					this.bodies.add(crate1);
					this.game.world.renderable = true;
						this.shadowTexture = this.game.add.bitmapData(this.game.width, this.game.height);
						var lightSprite = this.game.add.image(0, 0, this.shadowTexture);
						lightSprite.blendMode = Phaser.blendModes.MULTIPLY;
						this.LIGHT_RADIUS = 100;
						this.updateShadowTexture();
												lvl1msg =  this.game.add.sprite(748 ,(2 *48)+3, 'lvl1msg2'+BasicGame.current_level_id);
							lvl1msg.animations.add('play',[0,1,2,3,4], 2, false);
					//this.bodies.add(crate1.body);
							break;
						case 6: 
						    player = this.game.add.sprite(2 * 48, 6 * 48, 'character');
							mover1 = this.game.add.sprite(12*48, (1 *48)+3, 'mover');
							crate1 = this.game.add.sprite(16 * 48, 350, 'crate');
							plate1 = this.game.add.tileSprite(312 ,(9 *48)+3, 48, 7, 'pressurePlate');
							plate2 = this.game.add.tileSprite(648 ,(19 *48)+3, 48, 7, 'pressurePlate');
							endpoint = this.game.add.tileSprite(00 ,(96), 11, 96, 'end');
							this.createDoor1();
							this.createDoor2();
							this.game.world.setBounds(-480, -480, 960, 960);
							this.game.world.pivot.setTo(480, 480);
							doorhum = this.game.add.audio('doorHum');
							                    this.game.physics.p2.enable(player);
                    this.game.physics.p2.enable(crate1);
		lvl1msg =  this.game.add.sprite(1000 ,(2 *48)+3, 'lvl1msg2'+BasicGame.current_level_id);
							lvl1msg.animations.add('play',[0,1,2,3,4], 2, false);
                    this.game.physics.p2.enable(plate1);
                    this.game.physics.p2.enable(plate2)
					
					this.game.physics.p2.enable(mover1);
					
					this.game.physics.p2.enable(endpoint);
                    
                    //player.body.angle = 90;
                    player.body.fixedRotation = false;
                    crate1.body.fixedRotation = true;
					
                    plate1.body.fixedRotation = true;
                    plate2.body.fixedRotation = true;
					
					mover1.body.fixedRotation = true;
                    plate1.body.static = true;
                    plate2.body.static = true;
					
					endpoint.body.static = true;
					mover1.body.static = true;
					
                    
					mover1.animations.add("moving",[0,1,2],3, true);
					
                    player.animations.add('runR',[2,3,4,3],6,true);
                    player.animations.add('runL',[5,6,7,6],6,true);
                    player.animations.add('stand',[0]);

                    player.animations.add('runRU',[10,11,12,11],6,true);
                    player.animations.add('runLU',[13,14,15,14],6,true);
                    player.animations.add('standU',[8]);
                    
                    
                   
                   // player.body.bounce.y = 0;
                   
                   this.game.physics.p2.gravity.y = 380;
                    //this.game.physics.p2.gravity.x = 10000;
                    this.game.camera.follow(player);
                    
                    //this.game.physics.p2.setBoundsToWorld(true, true, true, true, false);                 
                    
                    
                    plate1.body.onBeginContact.add(this.openDoor1, this);
                    plate2.body.onBeginContact.add(this.openDoor2, this);
					
					 
					mover1.body.onBeginContact.add(this.move, this);
					mover1.body.onEndContact.add(this.stopMove, this);
					this.bodies = this.game.add.group();
					//var body = this.game.add.sprite(player);
					this.bodies.add(player);
					this.bodies.add(crate1);
					//this.bodies.add(crate1.body);
					
							break;
						case 7:
							player = this.game.add.sprite(2 * 48, 6 * 48, 'character');
							glowstick = this.game.add.sprite(10*48, 6*48, 'glowstick');
							crate1 = this.game.add.sprite(16 * 48, 350, 'crate');
							crate2 = this.game.add.sprite(17*48-15 ,(15 *48), 'crateH');
							crate3 = this.game.add.sprite(18*48 ,(15 *48), 'crateH');
							plate1 = this.game.add.tileSprite(312 ,(19 *48)+3, 48, 7, 'pressurePlate');
							plate2 = this.game.add.tileSprite(600 ,(19 *48)+3, 48, 7, 'pressurePlate');
							plate3 = this.game.add.tileSprite(480 ,(1 *48)-3, 48, 7, 'pressurePlate');
							plate4 = this.game.add.tileSprite(2*48 ,(1 *48)-3, 48, 7, 'pressurePlate');
							endpoint = this.game.add.tileSprite(804+96+58 ,(18 *48), 11, 96, 'end');
							mover1 = this.game.add.sprite(11.5*48, (3 *48)-3, 'mover');
							//mover2 = this.game.add.sprite(8*48, (1 *48)-3, 'mover');
							doorhum = this.game.add.audio('doorHum');
							this.createDoor1();
							this.createDoor2();
							this.createDoor3();
							this.createDoor4();
							this.game.world.setBounds(-480, -480, 960, 960);
							this.game.world.pivot.setTo(480, 480);
							
							 this.game.physics.p2.enable(player);
							 this.game.physics.p2.enable(glowstick);
                    this.game.physics.p2.enable(crate1);
					this.game.physics.p2.enable(crate2);
					this.game.physics.p2.enable(crate3);
                    this.game.physics.p2.enable(plate1);
                    this.game.physics.p2.enable(plate2)
					this.game.physics.p2.enable(plate3);
                    this.game.physics.p2.enable(plate4);
					this.game.physics.p2.enable(mover1);
					//this.game.physics.p2.enable(mover2);
					this.game.physics.p2.enable(endpoint);
                    
                    //player.body.angle = 90;
                    player.body.fixedRotation = false;
					glowstick.body.fixedRotation = false;
                    crate1.body.fixedRotation = true;
					crate2.body.fixedRotation = true;
					crate3.body.fixedRotation = true;
                    plate1.body.fixedRotation = true;
                    plate2.body.fixedRotation = true;
					plate3.body.fixedRotation = true;
                    plate4.body.fixedRotation = true;
					mover1.body.fixedRotation = true;
                    plate1.body.static = true;
                    plate2.body.static = true;
					plate3.body.static = true;
                    plate4.body.static = true;
					endpoint.body.static = true;
					mover1.body.static = true;
					//mover2.body.static = true;
                    
					mover1.animations.add("moving",[0, 1, 2],3, true);
					//mover2.animations.add("moving",[0, 1, 2],3, true);
                    player.animations.add('runR',[2,3,4,3],6,true);
                    player.animations.add('runL',[5,6,7,6],6,true);
                    player.animations.add('stand',[0]);

                    player.animations.add('runRU',[10,11,12,11],6,true);
                    player.animations.add('runLU',[13,14,15,14],6,true);
                    player.animations.add('standU',[8]);
                    
                    
                   
                   // player.body.bounce.y = 0;
                   
                   this.game.physics.p2.gravity.y = 380;
                    //this.game.physics.p2.gravity.x = 10000;
                    this.game.camera.follow(player);
                    
                    //this.game.physics.p2.setBoundsToWorld(true, true, true, true, false);                 
                    
                    
                    plate1.body.onBeginContact.add(this.openDoor1, this);
                    plate2.body.onBeginContact.add(this.openDoor2, this);
					plate3.body.onBeginContact.add(this.openDoor3, this);
                    plate4.body.onBeginContact.add(this.openDoor4, this);
					 
					mover1.body.onBeginContact.add(this.move, this);
					mover1.body.onEndContact.add(this.stopMove, this);
					
					//mover2.body.onBeginContact.add(this.move, this);
					//mover2.body.onEndContact.add(this.stopMove, this);
					
										this.bodies = this.game.add.group();
					//var body = this.game.add.sprite(player);
					this.bodies.add(player);
					this.bodies.add(crate1);
					this.bodies.add(crate2);
					this.bodies.add(crate3);
					this.game.world.renderable = true;
						this.shadowTexture = this.game.add.bitmapData(this.game.width, this.game.height);
						var lightSprite = this.game.add.image(0, 0, this.shadowTexture);
						lightSprite.blendMode = Phaser.blendModes.MULTIPLY;
						this.LIGHT_RADIUS = 100;
						this.updateShadowTexture();
					//this.bodies.add(crate1.body);
							break;
						case 8:
							player = this.game.add.sprite(312 ,(13 *48)+3, 'character');
							
							crate1 = this.game.add.sprite(16 * 48, 350, 'crate');
							crate2 = this.game.add.sprite(17*48-15 ,(15 *48), 'crateH');
							crate3 = this.game.add.sprite(14*48 ,(23*48), 'crateH');
							plate1 = this.game.add.tileSprite(312 ,(24 *48)+3, 48, 7, 'pressurePlate');
							plate2 = this.game.add.tileSprite(600 ,(1 *48)+3, 48, 7, 'pressurePlate');
							plate3 = this.game.add.tileSprite(480 ,(1 *48)-3, 48, 7, 'pressurePlate');
							plate4 = this.game.add.tileSprite(23*48 ,(17 *48)-2, 48, 7, 'pressurePlate');
							endpoint = this.game.add.tileSprite(804+96+58-432 ,(9 *48), 11, 96, 'end');
							mover1 = this.game.add.sprite(17.5*48, (9 *48)-4, 'mover');
							mover2 = this.game.add.sprite(17.5*48, (10 *48)-2, 'mover');
							doorhum = this.game.add.audio('doorHum');
							this.createDoor1();
							this.createDoor2();
							this.createDoor3();
							this.createDoor4();
							//this.createDoor5();
							this.game.world.setBounds(-600, -600, 1200, 1200);
							this.game.world.pivot.setTo(600, 600);
							
							                    this.game.physics.p2.enable(player);
                    this.game.physics.p2.enable(crate1);
					this.game.physics.p2.enable(crate2);
					this.game.physics.p2.enable(crate3);
                    this.game.physics.p2.enable(plate1);
                    this.game.physics.p2.enable(plate2)
					this.game.physics.p2.enable(plate3);
                    this.game.physics.p2.enable(plate4);
					this.game.physics.p2.enable(mover1);
					this.game.physics.p2.enable(mover2);
					this.game.physics.p2.enable(endpoint);
                    
                    //player.body.angle = 90;
                    player.body.fixedRotation = false;
                    crate1.body.fixedRotation = true;
					crate2.body.fixedRotation = true;
					crate3.body.fixedRotation = true;
                    plate1.body.fixedRotation = true;
                    plate2.body.fixedRotation = true;
					plate3.body.fixedRotation = true;
                    plate4.body.fixedRotation = true;
					mover1.body.fixedRotation = true;
                    plate1.body.static = true;
                    plate2.body.static = true;
					plate3.body.static = true;
                    plate4.body.static = true;
					endpoint.body.static = true;
					mover1.body.static = true;
					mover2.body.static = true;
                    
					
					mover1.animations.add("moving",[0, 1, 2],3, true);
					mover2.animations.add("moving",[0, 1, 2],3, true);
                    player.animations.add('runR',[2,3,4,3],6,true);
                    player.animations.add('runL',[5,6,7,6],6,true);
                    player.animations.add('stand',[0]);

                    player.animations.add('runRU',[10,11,12,11],6,true);
                    player.animations.add('runLU',[13,14,15,14],6,true);
                    player.animations.add('standU',[8]);
                    
                    
                   
                   // player.body.bounce.y = 0;
                   
                   this.game.physics.p2.gravity.y = 380;
                    //this.game.physics.p2.gravity.x = 10000;
                    this.game.camera.follow(player);
                    
                    //this.game.physics.p2.setBoundsToWorld(true, true, true, true, false);                 
                    
                    
                    plate1.body.onBeginContact.add(this.openDoor1, this);
                    plate2.body.onBeginContact.add(this.openDoor2, this);
					plate3.body.onBeginContact.add(this.openDoor3, this);
                    plate4.body.onBeginContact.add(this.openDoor4, this);
					 
					mover1.body.onBeginContact.add(this.move, this);
					mover1.body.onEndContact.add(this.stopMove, this);
					
					mover2.body.onBeginContact.add(this.move, this);
					mover2.body.onEndContact.add(this.stopMove, this);
					
															this.bodies = this.game.add.group();
					//var body = this.game.add.sprite(player);
					this.bodies.add(player);
					this.bodies.add(crate1);
					this.bodies.add(crate2);
					this.bodies.add(crate3);
							break;
					}

					this.game.input.onDown.add(this.unpause,this);

					//pause_label.inputEnabled = true;
				
					if (BasicGame.current_level_id !== 8){
						endpoint.body.onBeginContact.add(this.nextLevel, this);
					}
					else{
						endpoint.body.onBeginContact.add(this.endGame, this);
					}
                    cursors = this.game.input.keyboard.createCursorKeys();
                    grav = this.game.input.keyboard.addKey(Phaser.Keyboard.G);
                    rotR = this.game.input.keyboard.addKey(Phaser.Keyboard.D);
                    rotL = this.game.input.keyboard.addKey(Phaser.Keyboard.A);
					//countdown = this.game.add.text(970, 15, '01:00:00', { font: '24px Arial', fill: '#fff' });
					crateBtn = this.game.input.keyboard.addKey(Phaser.Keyboard.C);
					pauseBtn = this.game.input.keyboard.addKey(Phaser.Keyboard.P);
                    jumpButton = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
					torch = this.game.input.keyboard.addKey(Phaser.Keyboard.T);

					player.events.onOutOfBounds.add(this.kill, this);
					this.game.time.reset();

					//this.game.input.onDown.add(this.unpause, self);
					
	},
	
		kill: function(){
			console.log("dead");
		},
		//updateCountdown: function(){
			//var mins, secs, milis;
			//mins = 3 - Math.floor(this.game.time.now / 60000) % 60;
			//secs = 59 - Math.floor(this.game.time.now / 1000) % 60;
		//	milis = 99 - Math.floor(this.game.time.now) % 100;
		//	if (mins > -1){
		//	if (milis < 10)
			//	milis = '0' + milis;
 
			//if (secs < 10)
			//	secs = '0' + secs;
 
			//if (mins < 10)
			//	mins = '0' + mins;
			
			//countdown.setText(mins + ':'+ secs + ':' + milis);
			//}
			//else{this.showPause();}
		//},
	
		unpause: function(event){

			if(this.game.paused){
				// Calculate the corners of the menu
				var x1 = 960/2 - 270/2, x2 = 960/2 + 270/2,
                y1 = 960/2 - 180/2, y2 = 960/2 + 180/2;
				var choisemap = ['one', 'two', 'three', 'four', 'five', 'six'];
				var x = event.x - x1,
				y = event.y - y1;
				// Check if the click was inside the menu
				if(y >=4 && y <=86){
					if(x>=5 && x <=133){
						menu.destroy();
						choiseLabel.destroy();
						this.game.paused = false;
					}
					else if (x>=134 && x<=266)
					{	this.game.paused = false;
						this.game.time.update(0);
						this.state.restart('Game');}
					else{
						menu.destroy();
						choiseLabel.destroy();
						this.game.paused = false;
					}
				}
				else if(y>=87 && y<=175){
					if (x>=5 && x<=266)
					{this.game.paused = false;
						this.state.start('MainMenu');}
					else{
						menu.destroy();
						choiseLabel.destroy();
						this.game.paused = false;
					}
				}
				
				else{
					// Remove the menu and the label
					menu.destroy();
					choiseLabel.destroy();

					// Unpause the game
					this.game.paused = false;
				}
			}
		},
		showPause : function(){
			this.game.paused = true;
			switch (BasicGame.current_level_id){
				case 1:
					menu = this.game.add.sprite(480, 480, "pauseMenu");
					menu.anchor.setTo(0.5, 0.5);
					choiseLabel = this.game.add.text(960/2, 480-150, 'PAUSED. Click outside menu to continue', { font: '30px Arial', fill: '#fff' });
					choiseLabel.anchor.setTo(0.5, 0.5);
					break;
				case 2:
					menu = this.game.add.sprite(480, 480, "pauseMenu");
					menu.anchor.setTo(0.5, 0.5);
					choiseLabel = this.game.add.text(960/2, 480-150, 'PAUSED. Click outside menu to continue', { font: '30px Arial', fill: '#fff' });
					choiseLabel.anchor.setTo(0.5, 0.5);
					break;
				case 3:
					menu = this.game.add.sprite(480, 480, "pauseMenu");
					menu.anchor.setTo(0.5, 0.5);
					choiseLabel = this.game.add.text(960/2, 480-150, 'PAUSED. Click outside menu to continue', { font: '30px Arial', fill: '#fff' });
					choiseLabel.anchor.setTo(0.5, 0.5);
					break;
				case 4:
					menu = this.game.add.sprite(480, 480, "pauseMenu");
					menu.anchor.setTo(0.5, 0.5);
					choiseLabel = this.game.add.text(960/2, 480-150, 'PAUSED. Click outside menu to continue', { font: '30px Arial', fill: '#fff' });
					choiseLabel.anchor.setTo(0.5, 0.5);
					break;
				case 5:
					menu = this.game.add.sprite(480, 480, "pauseMenu");
					menu.anchor.setTo(0.5, 0.5);
					choiseLabel = this.game.add.text(960/2, 480-150, 'PAUSED. Click outside menu to continue', { font: '30px Arial', fill: '#fff' });
					choiseLabel.anchor.setTo(0.5, 0.5);
					break;
				case 6:
					menu = this.game.add.sprite(480, 480, "pauseMenu");
					menu.anchor.setTo(0.5, 0.5);
					choiseLabel = this.game.add.text(960/2, 480-150, 'PAUSED. Click outside menu to continue', { font: '30px Arial', fill: '#fff' });
					choiseLabel.anchor.setTo(0.5, 0.5);
					break;
				case 7:
					menu = this.game.add.sprite(480, 480, "pauseMenu");
					menu.anchor.setTo(0.5, 0.5);
					choiseLabel = this.game.add.text(960/2, 480-150, 'PAUSED. Click outside menu to continue', { font: '30px Arial', fill: '#fff' });
					choiseLabel.anchor.setTo(0.5, 0.5);
					break;
				case 8:
					menu = this.game.add.sprite(480, 480, "pauseMenu");
					menu.anchor.setTo(0.5, 0.5);
					choiseLabel = this.game.add.text(960/2, 480-150, 'PAUSED. Click outside menu to continue', { font: '30px Arial', fill: '#fff' });
					choiseLabel.anchor.setTo(0.5, 0.5);
					break;
			}
			
		},
		updateShadowTexture: function() {
 		
		    this.shadowTexture.context.fillStyle = 'rgb(7, 7, 7)';
			this.shadowTexture.context.fillRect(0, 0, this.game.width, this.game.height);

			// Draw circle of light
			    var gradient = this.shadowTexture.context.createRadialGradient(
        glowstick.body.x, glowstick.body.y, this.LIGHT_RADIUS * 0.75,
        glowstick.body.x, glowstick.body.y, this.LIGHT_RADIUS);
    gradient.addColorStop(0, 'rgba(46, 235, 17, 1.0)');
    gradient.addColorStop(1, 'rgba(46, 235, 17, 0.0)');
			this.shadowTexture.context.beginPath();
			this.shadowTexture.context.fillStyle = gradient;
			this.shadowTexture.context.arc(glowstick.body.x, glowstick.body.y,
				this.LIGHT_RADIUS, 0, Math.PI*2);
			this.shadowTexture.context.fill();

			// This just tells the engine it should update the texture cache
			this.shadowTexture.dirty = true;
			this.lightTimer = this.game.time.now + 1500;
		},
	
		move: function(body){
			this.onMover = body;
			this.needsToMove = true;

		},
		
		stopMove: function(body){
			this.needsToMove = false;
		},
		
        nextLevel: function(){
			console.log("Next Level!");
			this.door1Opened = false;
			this.door2Opened = false;
			this.door3Opened = false;
			this.door4Opened = false;
			this.reversed = false;
			this.msgPlayed = false;
			BasicGame.current_level_id++; //increment our level counter
			
			this.game.state.start("Game", true, false);
		},
		
		endGame: function() {
			console.log("Game Finished");
			this.game.world.rotation = 0;
			this.game.state.start("MainMenu", true, false);
		},
		
        openDoor1: function(sprite) {
             this.doorTimer = this.game.time.now + 5000;
             this.door1Opened = true;
		
			 this.opened = false;
        },
        
        openDoor2: function(sprite) {
             this.doorTimer = this.game.time.now + 5000;
             this.door2Opened = true;
			 this.opened = false;
        },
		openDoor3: function(sprite) {
             this.doorTimer = this.game.time.now + 5000;
             this.door3Opened = true;
			 this.opened = false;
        },
		openDoor4: function(sprite) {
             this.doorTimer = this.game.time.now + 5000;
             this.door4Opened = true;
			 this.opened = false;
        },
        
        createDoor1: function(){
			switch(BasicGame.current_level_id){
						case 6: 
							door1 = this.game.add.sprite(234 ,(2 *48), 'door2'); 
							door1.animations.add("open",[2,3 ,4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],8, false);
							break;
						case 7:
							door1 = this.game.add.sprite(18*48 ,(13 *48)+6, 'sDoor2'); 
							door1.animations.add("open",[2,3 ,4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],8, false);
							break;
						case 8:
							door1 = this.game.add.sprite(6*48 ,(15 *48)+6, 'sDoor2'); 
							door1.animations.add("open",[2,3 ,4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],8, false);
							break;
						case 3:
							door1 = this.game.add.sprite(17*48+6 ,(17 *48), 'door2'); 
							door1.animations.add("open",[2,3 ,4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],8, false);
							break;
						case 4:
							door1 = this.game.add.sprite(7*48 ,(11 *48), 'door2'); 
							door1.animations.add("open",[2,3 ,4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],8, false);
							break;
						case 5:
							door1 = this.game.add.sprite(4*48 ,(6 *48)+3, 'sDoor2'); 
							door1.animations.add("open",[2,3 ,4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],8, false);
							break;
			}
							this.game.physics.p2.enable(door1);
							door1.body.fixedRotation = true;
							door1.body.static = true;
        },
        
        createDoor2: function(){
			switch(BasicGame.current_level_id){
						case 6: 
							door2 = this.game.add.sprite(624-10 ,(8 *48), 'door2'); 
							 door2.animations.add("open",[2,3 ,4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],8, false);
							break;
						case 7:
							door2 = this.game.add.sprite(624-90 ,(18 *48), 'door2');
							door2.animations.add("open",[2,3 ,4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],5, false);							
							break;
						case 5:
							door2 = this.game.add.sprite(624 ,(6 *48)+3, 'sDoor2');
							door2.animations.add("open",[2,3 ,4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],5, false);							
							break;
						case 8:
							door2 = this.game.add.sprite(624+96+48 ,(23*48), 'door2');
							door2.animations.add("open",[2,3 ,4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],5, false);							
							break;
						case 3:
							door2 = this.game.add.sprite(912-(3*48) ,(23 *48), 'door2'); 
							door2.animations.add("open",[2,3 ,4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],8, false);
							break
			}
							this.game.physics.p2.enable(door2);
							door2.body.fixedRotation = true;
							door2.body.static = true;
        },
		createDoor3: function(){
					switch (BasicGame.current_level_id){
						case 7:
							door3 = this.game.add.sprite(624+48 ,(18 *48),  'door2'); 
							door3.animations.add("open",[2,3 ,4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],8, false);
							this.game.physics.p2.enable(door3);
							door3.body.fixedRotation = true;
							door3.body.static = true;
						break
						case 5:
							door3 = this.game.add.sprite(624+48 ,(18 *48),  'door2'); 
							door3.animations.add("open",[2,3 ,4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],8, false);
							this.game.physics.p2.enable(door3);
							door3.body.fixedRotation = true;
							door3.body.static = true;
						break
						case 8:
							door3 = this.game.add.sprite(624-90 ,(20 *48),  'door2'); 
							door3.animations.add("open",[2,3 ,4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],8, false);
							this.game.physics.p2.enable(door3);
							door3.body.fixedRotation = true;
							door3.body.static = true;
						break
					}

        },
		createDoor4: function(){
					switch (BasicGame.current_level_id){
						case 7:
							door4 = this.game.add.sprite(804-48 ,(18 *48),  'door2'); 
							door4.animations.add("open",[2,3 ,4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],8, false);
							this.game.physics.p2.enable(door4);
							door4.body.fixedRotation = true;
							door4.body.static = true;
							break
							case 5:
							door4 = this.game.add.sprite(804-48 ,(18 *48),  'door2'); 
							door4.animations.add("open",[2,3 ,4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],8, false);
							this.game.physics.p2.enable(door4);
							door4.body.fixedRotation = true;
							door4.body.static = true;
							break
						case 8:
							door4 = this.game.add.sprite(804-48 ,(9 *48),  'door2'); 
							door4.animations.add("open",[2,3 ,4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],8, false);
							this.game.physics.p2.enable(door4);
							door4.body.fixedRotation = true;
							door4.body.static = true;
							break
					}
        },
		
		createDoor5: function(){

							door5 = this.game.add.tileSprite(6*48 ,(15 *48)+6, 96, 11, 'sDoor2'); 
							door5.animations.add("open",[2,3 ,4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],8, false);
							this.game.physics.p2.enable(door5);
							door5.body.fixedRotation = true;
							door5.body.static = true;
        },

		destroyDoor1: function(){
			door2.destroy();
		},
        
	update: function () {
		

			//this.updateCountdown();
			if (BasicGame.current_level_id < 7 && this.msgPlayed === false){
				lvl1msg.animations.play('play');
			this.msgPlayed = true;}
		if (pauseBtn.isDown){
			this.showPause();
		
		}
		if (BasicGame.current_level_id === 7 || BasicGame.current_level_id === 5){
			if (torch.isDown && this.lightTimer < this.game.time.now){
			glowstick.body.x = player.body.x+15;
			glowstick.body.y = player.body.y+5;
			this.updateShadowTexture();}
		}
		if (BasicGame.current_level_id === 4){
		mover1.animations.play('moving');}
		//mover2.animations.play('moving');

		if (crateBtn.isDown && this.locked === false && this.pickupTimer < this.game.time.now)
		{
			if(Math.abs(player.body.x - crate1.body.x) < 60 && player.body.y+10 >= crate1.body.y ){
				this.crateLock = this.game.physics.p2.createDistanceConstraint(crate1, player, 50);
				this.crateDistance  = this.game.physics.p2.createLockConstraint(crate1, player, [0,-50], 0);
				 this.locked = true
				 
				 this.pickupTimer = this.game.time.now + 500;
			}
		}
		else if (crateBtn.isDown && this.locked === true && this.pickupTimer < this.game.time.now)	
		{
			 this.game.physics.p2.removeConstraint(this.crateLock);
			  this.game.physics.p2.removeConstraint(this.crateDistance);
			 this.locked = false;
			  this.pickupTimer = this.game.time.now + 500;
		}
		if (this.needsToMove === true){
				this.onMover.x +=5;	
		}
				
                if (this.doorTimer > this.game.time.now && this.opened === false){
                    if (this.door1Opened === true)
                    {
                        this.game.physics.p2.removeBody(door2.body); 
									 doorhum.play();
									 door2.animations.play('open');
									  //var timer = this.game.time.create(true);
									 // timer.add(2000, function(){door2.destroy();}, this);
									 // timer.start();
                        //door2.destroy();
					
                        //this.opened = true;
                        plate1.body.y += 1;
                    }
                    if (this.door2Opened === true)
                    {
                        this.game.physics.p2.removeBody(door1.body); 
									 doorhum.play();
									 door1.animations.play('open');
									  //var timer = this.game.time.create(true);
									 // timer.add(2000, function(){door1.destroy();}, this);
									 // timer.start();
                        //door1.destroy();
                  
                        //this.opened = true;
                        plate2.body.y += 2;
                    }
					if (this.door3Opened === true)
                    {
                        this.game.physics.p2.removeBody(door3.body);
									 doorhum.play();
									 door3.animations.play('open');
									 // var timer = this.game.time.create(true);
									//  timer.add(2000, function(){door3.destroy();}, this);
									//  timer.start();                  
                        //this.opened = true;
                        plate3.body.y -= 3;
                    }
					if (this.door4Opened === true)
                    {
                        this.game.physics.p2.removeBody(door4.body); 
									 									 doorhum.play();
									 door4.animations.play('open');
									//  var timer = this.game.time.create(true);
									//  timer.add(2000, function(){door4.destroy();}, this);
									//  timer.start();
                  
                       // this.opened = true;
                        plate4.body.y -= 3;
                    }
					this.opened = true;
                }
                else if (this.doorTimer < this.game.time.now && this.opened === true){
                    if (this.door1Opened === true)
                    {
                        this.createDoor2();
                        //this.opened = false;
                        plate1.body.y -= 2;
                        this.door1Opened = false;
                    }
                    if (this.door2Opened === true) {
                        this.createDoor1();
                       // this.opened = false;
                        plate2.body.y -= 2;
                        this.door2Opened = false;
                    }
					 if (this.door3Opened === true)
                    {
                        this.createDoor3();
                        //this.opened = false;
                        plate3.body.y += 3;
                        this.door3Opened = false;
                    }
					 if (this.door4Opened === true)
                    {
                        this.createDoor4();
                       // this.opened = false;
                        plate4.body.y += 3;
                        this.door4Opened = false;
                    }
					this.opened=false;
                }
                    
                    if (rotR.isDown && this.rotTimer < this.game.time.now){
                        this.time = this.game.time.now;  
                          this.anotherTimer = this.game.time.create(true);
                        switch (this.orientation){
                            case 0:
                                this.game.physics.p2.gravity.y = 0;
                                this.game.physics.p2.gravity.x = 0;
                                this.anotherTimer.loop(1, this.rotateRight, this);
                                this.anotherTimer.start();
                                
                                this.rotTimer = this.game.time.now + 1000;
                                break;
                            case 1:
                                this.game.physics.p2.gravity.y = 0;
                                this.game.physics.p2.gravity.x = 0;
                                this.anotherTimer.loop(1, this.rotateRight, this);
                                this.anotherTimer.start();
                                
//                     
                                this.rotTimer = this.game.time.now + 1000;
                                break;
                            case 2:
                                this.game.physics.p2.gravity.y = 0;
                                this.game.physics.p2.gravity.x = 0;
                                this.anotherTimer.loop(1, this.rotateRight, this);
                                this.anotherTimer.start();
                                
                                this.rotTimer = this.game.time.now + 1000;
                                break;
                            case 3:
                                this.game.physics.p2.gravity.y = 0;
                                this.game.physics.p2.gravity.x = 0;

                                this.anotherTimer.loop(1, this.rotateRight, this);
                                this.anotherTimer.start();
                                
                                this.rotTimer = this.game.time.now + 1000;
                                break;
                        }

                    }
                    else  if (rotL.isDown && this.rotTimer < this.game.time.now){
                        this.anotherTimer2 = this.game.time.create(true);
                           switch (this.orientation){
                            case 0:
                                this.game.physics.p2.gravity.y = 0;
                                this.game.physics.p2.gravity.x = 0;
                                this.anotherTimer2.loop(1, this.rotateLeft, this);
                                this.anotherTimer2.start();
                                
                                this.rotTimer = this.game.time.now + 1000;
                                break;
                            case 1:
                                this.game.physics.p2.gravity.y = 0;
                                this.game.physics.p2.gravity.x = 0;
                                this.anotherTimer2.loop(1, this.rotateLeft, this);
                                this.anotherTimer2.start();
                                
                                this.rotTimer = this.game.time.now + 1000;
                                break;
                            case 2:
                                this.game.physics.p2.gravity.y = 0;
                                this.game.physics.p2.gravity.x = 0;
                                this.anotherTimer2.loop(1, this.rotateLeft, this);
                                this.anotherTimer2.start();
                                
                                this.rotTimer = this.game.time.now + 1000;
                                break;
                            case 3:
                                this.game.physics.p2.gravity.y = 0;
                                this.game.physics.p2.gravity.x = 0;
                               this.anotherTimer2.loop(1, this.rotateLeft, this);
                                this.anotherTimer2.start();
                                
                                this.rotTimer = this.game.time.now + 1000;
                                break;
                        }
               
                    }
                    
                    if (grav.isDown && this.gravTimer < this.game.time.now){
                        if (this.reversed === false){
                            this.reversed = true;}
                        else{this.reversed = false;}
                        switch (this.orientation){
                           case 0:
                               this.game.physics.p2.gravity.y = this.game.physics.p2.gravity.y * -1;
                               this.game.physics.p2.gravity.x =0;
                               break;
                           case 1:
                               this.game.physics.p2.gravity.y = 0;
                               this.game.physics.p2.gravity.x = this.game.physics.p2.gravity.x * -1;
                               break;
                           case 2:
                               this.game.physics.p2.gravity.y = this.game.physics.p2.gravity.y * -1;
                               this.game.physics.p2.gravity.x =0;
                               break;
                           case 3:
                               this.game.physics.p2.gravity.y = 0;
                               this.game.physics.p2.gravity.x = this.game.physics.p2.gravity.x * -1;
                               break;
                        }
                       // this.vertMove = this.vertMove * -1; 
                        this.gravTimer = this.game.time.now + 1000;
						if (this.locked === true){
							this.game.physics.p2.removeConstraint(this.crateLock);
							this.game.physics.p2.removeConstraint(this.crateDistance);
							this.locked = false;
						}
						
                    }
                    
                    if (cursors.left.isDown)
                    {
                        switch (this.orientation) {
                            case 0:
                                player.body.moveLeft(200);
                                break;
                            case 1:
                                player.body.moveDown(200);
                                break;
                            case 2:
                                player.body.moveRight(200);
                                break;
                            case 3:
                                player.body.moveUp(200);
                                break;    
                        }
                        
                        //player.body.velocity.x = -this.hozMove;
                        if(this.reversed === false){
                        // Set the 'player' sprite's x velocity to a negative number:
                        // have it move left on the screen.
                        
                        player.animations.play('runL');
                        }
                        else{
                          
                        player.animations.play('runLU');  
                        }
                        facing = "left";
                    }
                    // Check if the right arrow key is being pressed
                    else if (cursors.right.isDown)
                    {   
                        switch (this.orientation) {
                            case 0:
                                player.body.moveRight(200);
                                break;
                            case 1:
                                player.body.moveUp(200);
                                break;
                            case 2:
                                player.body.moveLeft(200);
                                break;
                            case 3:
                                player.body.moveDown(200);
                                break;    
                        }
                        if(this.reversed === false){
                        // Set the 'player' sprite's x velocity to a negative number:
                        // have it move left on the screen.
                        
                        player.animations.play('runR');
                        }
                        else{
                          
                        player.animations.play('runRU');  
                        }
                     
                        facing = "right";
                    }
                    else{
                        if (this.reversed === false){
                        //THIS STOPS THE PLAYERS X GRAVITY WORKING PROPERLY
						if (this.side === false){
                        player.body.velocity.x = 0;
						}
						else{
							player.body.velocity.y =0;
						}
                        
                        player.animations.play('stand');
                        if (this.facing === "left") {
                        
                            player.frame = 1;
                        } else {
                            player.frame = 0;
                        }
                        
                    }
                    else{
                        //THIS STOPS THE PLAYERS X GRAVITY WORKING PROPERLY
                      if (this.side === false){
                        player.body.velocity.x = 0;
						}
						else{
							player.body.velocity.y =0;
						}
                        player.animations.play('standU');
                        if (this.facing === "left") {
                        
                            player.frame = 8;
                        } else {
                            player.frame = 9;
                        }
                        }
                    }
                                   
                                                //inner loop checks whether body is in contact with a surface on top or bottom - prevents double jumping
                                                // body.bottom cant be modified (read only) so using the onFloor method wouldnt work when gravity is reversed
                    if (jumpButton.isDown && this.game.time.now > this.jumpTimer)
                    {
                        
                        player.body.velocity.y = this.vertMove;
                       
                        this.jumpTimer = this.game.time.now + 650;
                         
                        
                    }
                    

	},
        
        rotateRight: function () {
            switch (this.orientation){
                case 0:
                    if (this.posRotation < 1.57){
						//player.body.velocity.x=0;
                       // crate1.body.velocity.x = 0;
						//crate2.body.velocity.x = 0;
						//crate3.body.velocity.x = 0;
                       // player.body.velocity.y=0;
                       // crate1.body.velocity.y=0;
						//crate2.body.velocity.y = 0;
						//crate3.body.velocity.y = 0;
						this.bodies.forEach(function(body){
							body.body.velocity.x = 0;
							body.body.velocity.y = 0;
						});
                        this.game.world.rotation += 0.02 ;
                        this.posRotation += 0.02;
                        
                    } 
                    else
                    {
                        this.anotherTimer.destroy();
                        this.orientation = 1;
						this.side = true;
                        player.body.angle -= 90;        
                        //player.body.velocity.x=0;
                        crate1.body.velocity.x = 0;
                        player.body.velocity.y=0;
                        crate1.body.velocity.y=0;
                        this.game.world.rotation = 1.571;
						
						if(this.reversed){
							this.game.physics.p2.gravity.x = -380;
						}
						else{
							this.game.physics.p2.gravity.x = 380;
						}
                        this.game.physics.p2.gravity.y = 0;
                        this.rotTimer = this.game.time.now + 1000;                
                    }
                    break;
                case 1:
                    if (this.posRotation < 3.135){
						//player.body.velocity.x=0;
                       //crate1.body.velocity.x = 0;
						//crate2.body.velocity.x = 0;
						//crate3.body.velocity.x = 0;
                       // player.body.velocity.y=0;
                       // crate1.body.velocity.y=0;
						//crate2.body.velocity.y = 0;
						//crate3.body.velocity.y = 0;
						this.bodies.forEach(function(body){
							body.body.velocity.x = 0;
							body.body.velocity.y = 0;
						});
                        this.game.world.rotation += 0.02 ;
                        this.posRotation += 0.02;
                    }    
                    else
                    {
                        this.anotherTimer.destroy();
                        this.orientation = 2;
						this.side = false;
                        player.body.angle -= 90; 
                        //player.body.velocity.x=0;
                        crate1.body.velocity.x = 0;
                        player.body.velocity.y=0;
                        crate1.body.velocity.y=0;
                        this.game.world.rotation = 3.142;
                        this.game.physics.p2.gravity.x = 0;
						
						if(this.reversed){
							this.game.physics.p2.gravity.y = 380;
						}
						else{
							this.game.physics.p2.gravity.y = -380;
						}
						
                        this.rotTimer = this.game.time.now + 1000;   
                    }
                    break;
                case 2:
                    if (this.posRotation < 4.7){
						//player.body.velocity.x=0;
                       //crate1.body.velocity.x = 0;
						//crate2.body.velocity.x = 0;
						//crate3.body.velocity.x = 0;
                       // player.body.velocity.y=0;
                       // crate1.body.velocity.y=0;
						//crate2.body.velocity.y = 0;
						//crate3.body.velocity.y = 0;
						this.bodies.forEach(function(body){
							body.body.velocity.x = 0;
							body.body.velocity.y = 0;
						});
                        this.game.world.rotation += 0.02 ;
                        this.posRotation += 0.02;
                    }                         
                    else
                    {
                        this.anotherTimer.destroy();
                        this.orientation = 3;
                        this.game.world.rotation = 4.713;
                        player.body.angle -= 90; 
                        //player.body.velocity.x=0;
                        crate1.body.velocity.x = 0;
                        player.body.velocity.y=0;
                        crate1.body.velocity.y=0;
						this.side = true;
                        if(this.reversed){
							this.game.physics.p2.gravity.x = 380;
						}
						else{
							this.game.physics.p2.gravity.x = -380;
						}
                        this.game.physics.p2.gravity.y = 0;
                        this.rotTimer = this.game.time.now + 1000;
                    }
                    break;
                case 3:
                    if (this.posRotation > 0){
						//player.body.velocity.x=0;
                       //crate1.body.velocity.x = 0;
						//crate2.body.velocity.x = 0;
						//crate3.body.velocity.x = 0;
                       // player.body.velocity.y=0;
                       // crate1.body.velocity.y=0;
						//crate2.body.velocity.y = 0;
						//crate3.body.velocity.y = 0;
						this.bodies.forEach(function(body){
							body.body.velocity.x = 0;
							body.body.velocity.y = 0;
						});
                        this.game.world.rotation += 0.02;
                        this.posRotation += -0.06;
                    }
                    else
                    {
                        this.anotherTimer.destroy();
                        this.orientation = 0;
                        this.posRotation = 0 ;
                        this.game.world.rotation = 0;
						this.side = false;
                        player.body.angle -= player.body.angle;
                       // player.body.velocity.x=0;
						player.body.velocity.x=0;
                        crate1.body.velocity.x = 0;
                        player.body.velocity.y=0;
                        crate1.body.velocity.y=0;
                
                        this.game.physics.p2.gravity.x = 0;
						if(this.reversed){
							this.game.physics.p2.gravity.y = -380;
						}
						else{
							this.game.physics.p2.gravity.y = 380;
						}
 
                        this.rotTimer = this.game.time.now + 1000;               
                    }
                    break;
                }
        },

        
        rotateLeft: function () {
            switch(this.orientation){
                case 0:
                    if (this.posRotation < 4.713){
						//player.body.velocity.x=0;
                       //crate1.body.velocity.x = 0;
						//crate2.body.velocity.x = 0;
						//crate3.body.velocity.x = 0;
                       // player.body.velocity.y=0;
                       // crate1.body.velocity.y=0;
						//crate2.body.velocity.y = 0;
						//crate3.body.velocity.y = 0;
						this.bodies.forEach(function(body){
							body.body.velocity.x = 0;
							body.body.velocity.y = 0;
						});
                        this.game.world.rotation += -0.02 ;
                        this.posRotation += 0.06;
                    } 
                    else
                    {
                        this.anotherTimer2.destroy();
                        this.orientation = 3;
						this.side = true;
                        player.body.angle += 90;        
                        //player.body.velocity.x=0;
                        crate1.body.velocity.x = 0;
                        player.body.velocity.y=0;
                        crate1.body.velocity.y=0;
                        this.game.world.rotation = 4.713;
                        if(this.reversed){
							this.game.physics.p2.gravity.x = 380;
						}
						else{
							this.game.physics.p2.gravity.x = -380;
						}
                        this.game.physics.p2.gravity.y = 0;
                        this.rotTimer = this.game.time.now + 1000;                
                    }
                break;
            
                case 1:
                    if (this.posRotation > 0){
						//player.body.velocity.x=0;
                       //crate1.body.velocity.x = 0;
						//crate2.body.velocity.x = 0;
						//crate3.body.velocity.x = 0;
                       // player.body.velocity.y=0;
                       // crate1.body.velocity.y=0;
						//crate2.body.velocity.y = 0;
						//crate3.body.velocity.y = 0;
						this.bodies.forEach(function(body){
							body.body.velocity.x = 0;
							body.body.velocity.y = 0;
						});
                        this.game.world.rotation += -0.02;
                        this.posRotation += -0.02;
                    }
                    else
                    {
                        this.anotherTimer2.destroy();
                        this.orientation = 0;
						this.side = false;
                        this.posRotation = 0 ;
                        this.game.world.rotation = 0;
                        player.body.angle -= player.body.angle;
                        //player.body.velocity.x=0;
						player.body.velocity.x=0;
                        crate1.body.velocity.x = 0;
                        player.body.velocity.y=0;
                        crate1.body.velocity.y=0;
                
                        this.game.physics.p2.gravity.x = 0;
                        if(this.reversed){
							this.game.physics.p2.gravity.y = -380;
						}
						else{
							this.game.physics.p2.gravity.y = 380;
						}
                        this.rotTimer = this.game.time.now + 1000;               
                    }
                    break;
                case 2:
                    if (this.posRotation > 1.57 ){
						//player.body.velocity.x=0;
                       //crate1.body.velocity.x = 0;
						//crate2.body.velocity.x = 0;
						//crate3.body.velocity.x = 0;
                       // player.body.velocity.y=0;
                       // crate1.body.velocity.y=0;
						//crate2.body.velocity.y = 0;
						//crate3.body.velocity.y = 0;
						this.bodies.forEach(function(body){
							body.body.velocity.x = 0;
							body.body.velocity.y = 0;
						});
                        this.game.world.rotation += -0.02 ;
                        this.posRotation += -0.02;
                    }                         
                    else
                    {
                        this.anotherTimer2.destroy();
                        this.orientation = 1;
						this.side=true;
                        this.game.world.rotation = 1.571;
                        player.body.angle += 90;  
                        //player.body.velocity.x=0;
                        crate1.body.velocity.x = 0;
                        player.body.velocity.y=0;
                        crate1.body.velocity.y=0;
                
                        if(this.reversed){
							this.game.physics.p2.gravity.x = -380;
						}
						else{
							this.game.physics.p2.gravity.x = 380;
						}
                        this.game.physics.p2.gravity.y = 0;
                        this.rotTimer = this.game.time.now + 1000;
                    }
                    break;
                case 3:
                    if (this.posRotation > 3.14){
						//player.body.velocity.x=0;
                       //crate1.body.velocity.x = 0;
						//crate2.body.velocity.x = 0;
						//crate3.body.velocity.x = 0;
                       // player.body.velocity.y=0;
                       // crate1.body.velocity.y=0;
						//crate2.body.velocity.y = 0;
						//crate3.body.velocity.y = 0;
						this.bodies.forEach(function(body){
							body.body.velocity.x = 0;
							body.body.velocity.y = 0;
						});
                        this.game.world.rotation += -0.02 ;
                        this.posRotation += -0.02;
                    }    
                    else
                    {
                        this.anotherTimer2.destroy();
                        this.orientation = 2;
                        this.game.world.rotation = 3.142;
                        player.body.angle += 90;  
                        //player.body.velocity.x=0;
                        crate1.body.velocity.x = 0;
                        player.body.velocity.y=0;
                        crate1.body.velocity.y=0;
                        this.side = false;
                        this.game.physics.p2.gravity.x = 0;
                       if(this.reversed){
							this.game.physics.p2.gravity.y = 380;
						}
						else{
							this.game.physics.p2.gravity.y = -380;
						}
                        this.rotTimer = this.game.time.now + 1000;   
                    }
                    break;
            }
            
        },
    render: function () {

		//	Here you should destroy anything you no longer need.
		//	Stop music, delete sprites, purge caches, free resources, all that good stuff.

		//	Then let's go back to the main menu.
		//this.game.debug.body(player);

	},
	quitGame: function (pointer) {

		//	Here you should destroy anything you no longer need.
		//	Stop music, delete sprites, purge caches, free resources, all that good stuff.

		//	Then let's go back to the main menu.
		this.state.start('MainMenu');

	}

};


