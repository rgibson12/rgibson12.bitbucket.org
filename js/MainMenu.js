
BasicGame.MainMenu = function (game) {
this.startButton;
};

BasicGame.MainMenu.prototype = {

	create: function () {

        console.log("Creating menu state!");
		BasicGame.current_level_id = 1;
		this.game.add.tileSprite(0,0, 960, 960, 'background', 0);
this.startButton = this.add.button(400, 370, 'startButton', this.startClick, this, 1,0);
		
	},

	update: function () {

		

	},
	startClick: function(){
		this.state.start('Game');
		
	},

	resize: function (width, height) {

		//	If the game container is resized this function will be called automatically.
		//	You can use it to align sprites that should be fixed in place and other responsive display things.

	}

};
