
BasicGame.Preloader = function (game) {

	this.background = null;
	this.preloadBar = null;

	this.ready = false;

};

BasicGame.Preloader.prototype = {

	preload: function () {

		 // Load the 'map.json' file using the TILDED_JSON special flag
                    this.game.load.tilemap('map', 'assets/challenging.json', null, Phaser.Tilemap.TILED_JSON);
                    this.game.load.tilemap('mapR', 'assets/challengingR.json', null, Phaser.Tilemap.TILED_JSON);
                    this.game.load.tilemap('mapU', 'assets/challengingU.json', null, Phaser.Tilemap.TILED_JSON);
                   this.game.load.tilemap('mapL', 'assets/challengingL.json', null, Phaser.Tilemap.TILED_JSON);
                    // Load the image 'level.png' and associate it in the cache as 'level'
                    this.game.load.image('level', 'assets/level2.png');
                    this.game.load.image('levelR', 'assets/levelR.png');
                    this.game.load.image('levelU', 'assets/levelU.png');
                    this.game.load.image('levelL', 'assets/levelL.png');
                    this.game.load.image('rectangle', 'assets/rectangle.png');
                    // Load the spritesheet 'character.png', telling Phaser each frame is 30x48
                   this.game.load.spritesheet('character', 'assets/spritesheet2U.png', 30, 49);
                    this.game.load.spritesheet('crate', 'assets/crate.png', 45, 45);
                   this.game.load.image("background", "assets/backdrop3window.png");
                    //game.load.spritesheet('character', 'assets/spritesheet.png', 111, 49);

	},

	create: function () {

        console.log("Creating preload state!");

		this.state.start('MainMenu');

	}

};
