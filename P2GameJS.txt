
BasicGame.Game = function (game) {

	//	When a State is added to Phaser it automatically has the following properties set on it, even if they already exist:

    this.game;		//	a reference to the currently running game
    this.add;		//	used to add sprites, text, groups, etc
    this.camera;	//	a reference to the game camera
    this.cache;		//	the game cache
    this.input;		//	the global input manager (you can access this.input.keyboard, this.input.mouse, as well from it)
    this.load;		//	for preloading assets
    this.math;		//	lots of useful common math operations
    this.sound;		//	the sound manager - add a sound, play one, set-up markers, etc
    this.stage;		//	the game stage
    this.time;		//	the clock
    this.tweens;    //  the tween manager
    this.state;	    //	the state manager
    this.world;		//	the game world
    this.particles;	//	the particle manager
    this.physics;	//	the physics manager
    this.rnd;		//	the repeatable random number generator
    
               this.map;
               this.newMap;
               this.posRotation =0;
               this.reversed = false;
                this.rotR;
                this.orientation = 0;
                this.rotL;
                this.rotValue =0;
                this.layer; // A layer within a tileset
                this.player; // The player-controller sprite
               this.facing = "left"; // Which direction the character is facing (default is 'left')
                this.cursors; // A reference to the keys to use for input detection
                this.jumpButton; // A reference to the button used for jumping
                this.grav;
                this.time;
                this.hozMove = 120; // The amount to move horizontally
                this.vertMove = -300; // The amount to move vertically (when 'jumping')
                this.jumpTimer = 0; 
                this.gravTimer = 0;
                this.rectangle;
                this.anotherTimer =0;
                  this.anotherTimer2 =0;
                this.rotTimer = 0;// The initial value of the timer

    //	You can use any of these from any function within this State.
    //	But do consider them as being 'reserved words', i.e. don't create a property for your own game called "world" or you'll over-write the world reference.

};

BasicGame.Game.prototype = {
                

	create: function () {
            
       
      
        console.log("Creating game state!");
        this.game.add.tileSprite(0,0, 624, 624, 'background', 0);
        //this.game.world.pivot.setTo(50, 50);
        //console.log(this.game.world.pivot);
        
                    this.game.physics.startSystem(Phaser.Physics.P2JS);
                    
                    map = this.game.add.tilemap('map');
                    map.addTilesetImage('level');

                    layer = map.createLayer('Tile Layer 1');
                    layer.resizeWorld();
                    
                    map.setCollisionBetween(1, 5);
                    
                    
                    
                 this.game.physics.p2.convertTilemap(map, layer);
          
                
                    //rectangle = this.game.add.sprite(290, 290, 'rectangle');  
                    player = this.game.add.sprite(2 * 48, 6 * 48, 'character');
                    this.game.physics.p2.enable(player);
                    player.body.fixedRotation = false;

                    player.animations.add('runR',[2,3,4,3],6,true);
                    player.animations.add('runL',[5,6,7,6],6,true);
                    player.animations.add('stand',[0]);

                    player.animations.add('runRU',[10,11,12,11],6,true);
                    player.animations.add('runLU',[13,14,15,14],6,true);
                    player.animations.add('standU',[8]);
                    this.game.world.setBounds(-312, -312, 624, 624);
                    this.game.world.pivot.setTo(312, 312);
                    
                   
                   // player.body.bounce.y = 0;
                   this.game.physics.p2.gravity.y = 380;
                    //this.game.physics.p2.gravity.x = 10000;
                    this.game.camera.follow(player);
                    
                    //this.game.physics.p2.setBoundsToWorld(true, true, true, true, false);                 
                    

                    cursors = this.game.input.keyboard.createCursorKeys();
                    grav = this.game.input.keyboard.addKey(Phaser.Keyboard.G);
                    rotR = this.game.input.keyboard.addKey(Phaser.Keyboard.D);
                    rotL = this.game.input.keyboard.addKey(Phaser.Keyboard.A);

                    jumpButton = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);

		//	Honestly, just about anything could go here. It's YOUR game after all. Eat your heart out!

	},

	update: function () {

		//this.game.physics.p2.collide(player, layer);
                    player.body.velocity.x = 0;
                    // Check if the left arrow key is being pressed
                    
                    if (rotR.isDown && this.rotTimer < this.game.time.now){
                        //this.game.physics.p2.clearTilemapLayerBodies(map, layer);
                        //map.destroy();
//                       layer.destroy(); //remove image of previous layer
//                       
//                    //*******************************************************  
//                       //attempt to remove previous collision bodies
//                      // map.setCollisionBetween(0,0); //set colliding tiles to none
//                       //layer = map.createLayer('Tile Layer 1'); //reset layer to use map with no colliding tiles
//                       //layer.resizeWorld();
//                       //this.game.physics.p2.convertTilemap(map, layer); // reset bodies with no tiles - THIS DOESNT WORK REGARDLESS OF COLLISIOS-RANGE SET
//                    //*******************************************************   
                        this.time = this.game.time.now;  
                          this.anotherTimer = this.game.time.create(true);
                        switch (this.orientation){
                            case 0:
                                this.game.physics.p2.gravity.y = 0;
                                this.game.physics.p2.gravity.x = 0;
                                this.anotherTimer.loop(1, this.doTheRot1, this);
                                this.anotherTimer.start();
                               
                                this.rotTimer = this.game.time.now + 1000;
                                break;
                            case 1:
                                this.game.physics.p2.gravity.y = 0;
                                this.game.physics.p2.gravity.x = 0;
                                this.anotherTimer.loop(1, this.doTheRot2, this);
                                this.anotherTimer.start();
                                
//                     
                                this.rotTimer = this.game.time.now + 1000;
                                break;
                            case 2:
                                this.game.physics.p2.gravity.y = 0;
                                this.game.physics.p2.gravity.x = 0;
                                this.anotherTimer.loop(1, this.doTheRot3, this);
                                this.anotherTimer.start();
                                
                                this.rotTimer = this.game.time.now + 1000;
                                break;
                            case 3:
                                this.game.physics.p2.gravity.y = 0;
                                this.game.physics.p2.gravity.x = 0;
//                               while (this.posRotation > 0){                  
//                                this.game.world.rotation -= 0.001 ;
//                                this.posRotation -= 0.001;
//                                player.body.rotation += 0.001;
//                                }
//                                player.body.velocity.x = 0;
//                                player.body.velocity.y = 0;
//                                this.game.physics.p2.gravity.x = 0;
//                                this.game.physics.p2.gravity.y = 380;
//                                this.rotTimer = this.game.time.now + 1000;
//                        
//                              
//                                this.orientation = 0; 
//                                this.rotTimer = this.game.time.now + 1000;
//                                break;
                                this.anotherTimer.loop(1, this.doTheRot5, this);
                                this.anotherTimer.start();
                                
                                this.rotTimer = this.game.time.now + 1000;
                                break;
                        }
//                        
//                        map.addTilesetImage('level');
//
//                        layer = map.createLayer('Tile Layer 1');
//                        layer.resizeWorld();                       
//                        
//                        map.setCollisionBetween(1, 1);
//                        this.game.physics.p2.convertTilemap(map, layer);
                        //this destroys established bodies b1efore setting new ones - should stop the "invisible map" bug but DOESNT!!
                        
//                        while (this.game.world.rotation < 1.571){
//                            //if(this.anotherTimer < this.game.time.now){
//                                this.game.world.rotation += 0.001 ;
//                                player.body.rotation -= 0.001;
//                                //this.anotherTimer = this.game.time.now + 50;
//                            //}
//                        }
//                    //if (this.reversed === false){
////                        this.reversed = true;
////                    }
////                    else{this.reversed = false;}
//                        this.game.physics.p2.gravity.x = 10000;
//                        this.game.physics.p2.gravity.y = 0;
//                        this.rotTimer = this.game.time.now + 1000;
//                        
                    }
                    else  if (rotL.isDown && this.rotTimer < this.game.time.now){
                        this.anotherTimer2 = this.game.time.create(true);
                           switch (this.orientation){
                            case 0:
                                this.game.physics.p2.gravity.y = 0;
                                this.game.physics.p2.gravity.x = 0;
                                this.anotherTimer2.loop(1, this.doTheRot6, this);
                                this.anotherTimer2.start();
                                
                                this.rotTimer = this.game.time.now + 1000;
                                break;
                            case 1:
                                this.game.physics.p2.gravity.y = 0;
                                this.game.physics.p2.gravity.x = 0;
                                this.anotherTimer2.loop(1, this.doTheRot9, this);
                                this.anotherTimer2.start();
                                
                                this.rotTimer = this.game.time.now + 1000;
                                break;
                            case 2:
                                this.game.physics.p2.gravity.y = 0;
                                this.game.physics.p2.gravity.x = 0;
                                this.anotherTimer2.loop(1, this.doTheRot8, this);
                                this.anotherTimer2.start();
                                
                                this.rotTimer = this.game.time.now + 1000;
                                break;
                            case 3:
                                this.game.physics.p2.gravity.y = 0;
                                this.game.physics.p2.gravity.x = 0;
                               this.anotherTimer2.loop(1, this.doTheRot7, this);
                                this.anotherTimer2.start();
                                
                                this.rotTimer = this.game.time.now + 1000;
                                break;
                        }
//                        //map.destroy();
//                        layer.destroy();
//                        switch (this.orientation){
//                            case 0:
//                              map = this.game.add.tilemap('mapL');
//                              this.orientation = 3; 
//                              this.rotTimer = this.game.time.now + 1000;
//                              break;
//                            case 1:
//                                map = this.game.add.tilemap('map');
//                                this.orientation = 0;
//                                this.rotTimer = this.game.time.now + 1000;
//                                break;
//                            case 2:
//                               map = this.game.add.tilemap('mapR');
//                                this.orientation = 1;
//                                this.rotTimer = this.game.time.now + 1000;
//                                break;
//                            case 3:
//                               map = this.game.add.tilemap('mapU');
//                                this.orientation = 2;
//                                this.rotTimer = this.game.time.now + 1000;
//                                break;
//                        }
//                        
//                       
//                        
//                        map.addTilesetImage('level');
//
//                        layer = map.createLayer('Tile Layer 1');
//                        layer.resizeWorld();
//                        
//                        map.setCollisionBetween(1, 5);
//                        this.game.physics.p2.convertTilemap(map, layer);
//                        
              
            
               
                    }
                    
                    if (grav.isDown && this.gravTimer < this.game.time.now){
                        if (this.reversed === false){
                       this.reversed = true;}
                       else{this.reversed = false;}
                       this.game.physics.p2.gravity.y = this.game.physics.p2.gravity.y * -1;
                       this.vertMove = this.vertMove * -1; 
                       this.gravTimer = this.game.time.now + 1000;
                       //player.body.bottom = -player.body.bottom;
                       
                       
                       //BOTTOM IS READ ONLY !!
                    }
                    
                    if (cursors.left.isDown)
                    {
                        switch (this.orientation) {
                            case 0:
                                player.body.moveLeft(200);
                                break;
                            case 1:
                                player.body.moveDown(200);
                                break;
                            case 2:
                                player.body.moveRight(200);
                                break;
                            case 3:
                                player.body.moveUp(200);
                                break;    
                        }
                        
                        //player.body.velocity.x = -this.hozMove;
                        if(this.reversed === false){
                        // Set the 'player' sprite's x velocity to a negative number:
                        // have it move left on the screen.
                        
                        player.animations.play('runL');
                        }
                        else{
                          
                        player.animations.play('runLU');  
                        }
                        facing = "left";
                    }
                    // Check if the right arrow key is being pressed
                    else if (cursors.right.isDown)
                    {   
                        switch (this.orientation) {
                            case 0:
                                player.body.moveRight(200);
                                break;
                            case 1:
                                player.body.moveUp(200);
                                break;
                            case 2:
                                player.body.moveLeft(200);
                                break;
                            case 3:
                                player.body.moveDown(200);
                                break;    
                        }
                        if(this.reversed === false){
                        // Set the 'player' sprite's x velocity to a negative number:
                        // have it move left on the screen.
                        
                        player.animations.play('runR');
                        }
                        else{
                          
                        player.animations.play('runRU');  
                        }
                     
                        facing = "right";
                    }
                    else{
                        if (this.reversed === false){
                        player.body.velocity.x=0;
                        player.animations.play('stand');
                        if (this.facing === "left") {
                        
                            player.frame = 1;
                        } else {
                            player.frame = 0;
                        }
                        
                    }
                    else{
                            player.body.velocity.x=0;
                        player.animations.play('standU');
                        if (this.facing === "left") {
                        
                            player.frame = 8;
                        } else {
                            player.frame = 9;
                        }
                        }
                    }
                                   
                                                //inner loop checks whether body is in contact with a surface on top or bottom - prevents double jumping
                                                // body.bottom cant be modified (read only) so using the onFloor method wouldnt work when gravity is reversed
                    if (jumpButton.isDown && this.game.time.now > this.jumpTimer)
                    {
                        
                        player.body.velocity.y = this.vertMove;
                       
                        this.jumpTimer = this.game.time.now + 650;
                         
                        
                    }
                    

	},
        
        doTheRot1: function () {
            if (this.posRotation < 1.57){
                player.body.velocity.x=0;
                player.body.velocity.y=0;
                this.game.world.rotation += 0.02 ;
                this.posRotation += 0.02;
            } 
            else
            {
                this.anotherTimer.destroy();
                this.orientation = 1;
               
                player.body.rotation -= 1.57;        
                player.body.velocity.x = 0;
                player.body.velocity.y = 0;
                this.game.world.rotation = 1.571;
                this.game.physics.p2.gravity.x = 10000;
                this.game.physics.p2.gravity.y = 0;
                this.rotTimer = this.game.time.now + 1000;                
            }
        },
        
        doTheRot2: function () {
            if (this.posRotation < 3.135){
                player.body.velocity.x=0;
                player.body.velocity.y=0;
                this.game.world.rotation += 0.02 ;
                this.posRotation += 0.02;
            }    
            else
            {
                this.anotherTimer.destroy();
                this.orientation = 2;
                
                player.body.rotation -= 1.57;
                player.body.velocity.x = 0;
                player.body.velocity.y = 0;
                this.game.world.rotation = 3.142;
                this.game.physics.p2.gravity.x = 0;
                this.game.physics.p2.gravity.y = -380;
                this.rotTimer = this.game.time.now + 1000;   
            }
        },
        
        doTheRot3: function () {
            
            if (this.posRotation < 4.7){
                player.body.velocity.x=0;
                player.body.velocity.y=0;
                this.game.world.rotation += 0.02 ;
                this.posRotation += 0.02;
            }                         
            else
            {
                this.anotherTimer.destroy();
                this.orientation = 3;
                this.game.world.rotation = 4.713;
                player.body.rotation -= 1.57;
                player.body.velocity.x = 0;
                player.body.velocity.y = 0;
                
                this.game.physics.p2.gravity.x = -10000;
                this.game.physics.p2.gravity.y = 0;
                this.rotTimer = this.game.time.now + 1000;
            }
        },
        
        doTheRot5: function (){
            console.log(this.posRotation);
            if (this.posRotation > 0){
                this.game.world.rotation += 0.02;
                this.posRotation += -0.06;
                //console.log(this.game.world.rotation);
            }
            else
            {
                this.anotherTimer.destroy();
                this.orientation = 0;
                this.posRotation = 0 ;
                this.game.world.rotation = 0;
                
                player.body.rotation -= player.body.rotation;
                player.body.velocity.x = 0;
                player.body.velocity.y = 0;
                
                this.game.physics.p2.gravity.x = 0;
                this.game.physics.p2.gravity.y = 380;
                this.rotTimer = this.game.time.now + 1000;               
            }
        },
        
        doTheRot6: function () {
            if (this.posRotation < 4.713){
                player.body.velocity.x=0;
                player.body.velocity.y=0;
                this.game.world.rotation += -0.02 ;
                this.posRotation += 0.06;
            } 
            else
            {
                this.anotherTimer2.destroy();
                this.orientation = 3;
               
                player.body.rotation += 1.57;        
                player.body.velocity.x = 0;
                player.body.velocity.y = 0;
                this.game.world.rotation = 4.713;
                this.game.physics.p2.gravity.x = -10000;
                this.game.physics.p2.gravity.y = 0;
                this.rotTimer = this.game.time.now + 1000;                
            }
            
            
        },
        
        doTheRot7: function () {
            if (this.posRotation > 3.14){
                player.body.velocity.x=0;
                player.body.velocity.y=0;
                this.game.world.rotation += -0.02 ;
                this.posRotation += -0.02;
            }    
            else
            {
                this.anotherTimer2.destroy();
                this.orientation = 2;
                this.game.world.rotation = 3.142;
                player.body.rotation += 1.57;
                player.body.velocity.x = 0;
                player.body.velocity.y = 0;
                        
                this.game.physics.p2.gravity.x = 0;
                this.game.physics.p2.gravity.y = -380;
                this.rotTimer = this.game.time.now + 1000;   
            }
        },
        
        doTheRot8: function () {
            
            if (this.posRotation > 1.57 ){
                player.body.velocity.x=0;
                player.body.velocity.y=0;
                this.game.world.rotation += -0.02 ;
                this.posRotation += -0.02;
            }                         
            else
            {
                this.anotherTimer2.destroy();
                this.orientation = 1;
                this.game.world.rotation = 1.571;
                player.body.rotation += 1.57;
                player.body.velocity.x = 0;
                player.body.velocity.y = 0;
                
                this.game.physics.p2.gravity.x = 10000;
                this.game.physics.p2.gravity.y = 0;
                this.rotTimer = this.game.time.now + 1000;
            }
        },
        
        doTheRot9: function (){
            //console.log(this.posRotation);
            if (this.posRotation > 0){
                this.game.world.rotation += -0.02;
                this.posRotation += -0.02;
                //console.log(this.game.world.rotation);
            }
            else
            {
                this.anotherTimer2.destroy();
                this.orientation = 0;
                this.posRotation = 0 ;
                this.game.world.rotation = 0;
                player.body.rotation -= player.body.rotation;
                player.body.velocity.x = 0;
                player.body.velocity.y = 0;
                
                this.game.physics.p2.gravity.x = 0;
                this.game.physics.p2.gravity.y = 380;
                this.rotTimer = this.game.time.now + 1000;               
            }
        },
        
        checkIfCanJump: function (result) {

            var yAxis = p2.vec2.fromValues(0, 1);
            result = false;

            for (var i = 0; i < game.physics.p2.world.narrowphase.contactEquations.length; i++)
            {
                var c = game.physics.p2.world.narrowphase.contactEquations[i];
         
                if (c.bodyA === player.body.data || c.bodyB === player.body.data)
                {
                    var d = p2.vec2.dot(c.normalA, yAxis); // Normal dot Y-axis
                    if (c.bodyA === player.body.data) d *= -1;
                    if (d > 0.5) result = true;
                }   
            }
    
            return result;

        },

    render: function () {

		//	Here you should destroy anything you no longer need.
		//	Stop music, delete sprites, purge caches, free resources, all that good stuff.

		//	Then let's go back to the main menu.
		//this.game.debug.body(player);

	},
	quitGame: function (pointer) {

		//	Here you should destroy anything you no longer need.
		//	Stop music, delete sprites, purge caches, free resources, all that good stuff.

		//	Then let's go back to the main menu.
		this.state.start('MainMenu');

	}

};


